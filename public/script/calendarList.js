	var Path;
   	var Labels;
   	var UserId;
   	var myTask;
	var assgndTsk;
	var historyTsk;
	var lighttpdpath;
	var userType;
	
	var title = ""; 
	var start = "";
	var end = "";
	var stDate="";
	var startTime = "";
	var endTime = "";
	var repeat = "";
	var description = "";
	var update='';
	var eId='';	
	var calendar = null;
	var curr_event = null;
	var count = 0;
	var priorityId="";
	var modelId="";
	var mid = "";
	var projId = 0;
	var reminder = "";
	var reminderType= "";
	var selectedTaskemailId="";
	var selectedOtherEmail="";
	var removeEmailId="";
	var parId="";
	var myTaskId="";
	var docTaskType="";
	var headerText="";
	var width="";
	var height="";
	var isHistory="";
	var recPatternValue = '';
	var recStart = '';
	var recEnd = '';
	var recType = '';
	var endId="noEnd";
	var sortTaskType='';
	var limit=0;
	var stepVal='';
	
	var serverHour = '';
	var isiPad = '';
	var searchWord = '';
	
	var tmpSDate = "";		       /* view task variables */
	var tmpEDate = "";
	var taskHour = "";
	var aHour = "";
	
	var latestCmntDate='';	       /* update Assigned task variables*/
	var taskType1 = '';
	var reminder = '';
	var remType = '';
	
	var tAction = '';		       /* updateMyTask variables */
	var myComment = '';
	var taskCompAmount = '';
	var workingHours = '';
	var taskAction = '';
	var cmntDate='';
	var cmntId='';
	
	var commentXml='<Comments>';   /* variable for cancelUpdate*/
	var valueStatus="";
	
	var cntntVal = '';			   /* addParticipantEmail method variable*/

	var tmpSDate = "";			   /* varable for updateEpicTasks method*/
	var tmpEDate = "";
	var taskHour = "";
	var aHour = "";
	
	var saveTaskFlag;              /*variable for testEstimatedHour method */
	
	var tskId = '';				   /*   variable for deleteEvent  */
	var tskType ='';	
	
	var userEmailId='';			   /* removeUpdateParticipant */
	var delUserId='';
	var delTaskId='';
	var objVal='';					
	
	var deletedUserID=",";         /* removeTaskUpdateParticipant vaiable*/
 	var sprintUId = '';
	
	var removeotherEmailId='';	   /* removeOtherPrtcipant variable  */
	var otherObj='';
	
	var startDate = '';			  /*variable for showTaskHoursPopup*/
    var endDate = '';	
    
    var resetDate = 0;		      /* variable for assignedDateReset*/	
      
    var prevSDate='';			  /* variable for loadStoryDate */	
 	var prevEDate='';  	
 	
 	var taskDateXml="";		      /* variable for getDateXml */
	var flag1='0';
	var confirmId ='';
	var totalHr='';
	var estmHour=0;
	var estmMinute=0;		 
	
	var documentId = '';					/* variables for downloadFile */			
	
	var messageUserId = '';				/*variables for openMessagePopup */
		
	
	function setPath(path,uId){
		Path = path;
		UserId=uId;
	}
	function setLabels(labels){
		Labels=labels
	}
	function setLightTpdPath(lightTpdpath){
		lighttpdpath=lightTpdpath;
	}
	function setUserRegType(userRegType){
		userType=userRegType;
	}

var myTaskScroll = true;
function myTaskListScroll(sortVal,sortTType,searchWord) {
	$("#mytaskdatalist").mCustomScrollbar({
		  scrollButtons:{
			  enable:true
		  },
		  callbacks:{
	        onTotalScroll:function(){if(sortTType=="All" || sortTType=="" || sortTType=="all"){
	            parent.$("#loadingBar").show();
			    parent.timerControl("start");
			    limit = limit+20;
			    if(myTaskScroll == false) {
			    	parent.$("#loadingBar").hide();
					parent.timerControl("");
			    }
			    else {
			    	$.ajax({ 
				       url: Path+'/calenderAction.do',
				       type:"POST",
					   data:{act:'FetchMyTaskListViewData',limit:limit,myTaskSortValue:sortVal,sortTaskType:sortTType,searchWord:searchWord},
					   beforeSend: function (jqXHR, settings) {
					        xhrPool.push(jqXHR);
					    },
					   error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
								}, 
					   success:function(result){
					   		parent.checkSessionTimeOut(result);
					   		if(result == '<div id="MytaskList" class="listViewDatas"></div>'){
					   			myTaskScroll = false;
					   			parent.$("#loadingBar").hide();
								parent.timerControl("");
					   		}
					   		else{
					   			myTaskScroll = true;
						   		$("#MytaskList").append(result);
								var newIds="";
					            var idArray=taskNotType.split(',');
					            $(".taskSub, .taskTypeImg").each(function(){
				                	var title = $(this).attr('title').replaceAll("CHR(41)"," ");
				                	$(this).attr('title',title);
								});
								var isiPad = navigator.userAgent.match(/iPad/i) != null;
					        	if(!isiPad)
					            	$("#mytaskdatalist").mCustomScrollbar("update");
					            else
					            	$('#mytaskheaders').css('width','100%');
					            $('#myTaskImg').css('opacity','1');
					            parent.$("#loadingBar").hide();
					            parent.timerControl("");
					        }   
						}
				     });
			     }}
	        },
	        onTotalScrollOffset:100
		  }
	  });	
	  $('div#MytaskList .mCSB_container').css('margin-right','15px');
	  $('#mytaskdatalist div div.mCSB_container').css('width','98.2%');
	  $('#mytaskdatalist div div.mCSB_scrollTools').css('margin-left','0.5%');	  
}
var assignedScroll=true;
function assignedTaskListScroll(sortVal,sortTType) {
	$("#assignedtaskdatalist").mCustomScrollbar({
		  scrollButtons:{
			  enable:true
		  },callbacks:{
	        onTotalScroll:function(){if(sortTType=="All" || sortTType=="" || sortTType=="all"){
	            parent.$("#loadingBar").show();
			    parent.timerControl("start");
			    limit = limit+20;
			    if(assignedScroll == false) {
			    	parent.$("#loadingBar").hide();
					parent.timerControl("");
			    }
			    else {
			    	$.ajax({ 
				       url: Path+'/calenderAction.do',
				       type:"POST",
					   data:{act:'FetchAssignedTaskListViewData',limit:limit,assignedTaskSortValue:sortVal,sortTaskType:sortTType},
					   error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
								}, 
					   success:function(result){
					     parent.checkSessionTimeOut(result);
					     var data = result.split("~assginTask~");
					     if(data[1] == '<div id="AssignedtaskList" class="listViewDatas"></div>'){
			     			assignedScroll = false;
				   			parent.$("#loadingBar").hide();
							parent.timerControl("");
						 }
						 else {
						 	 assignedScroll = true;
						     $("#AssignedtaskList").append(data[1]);
						     $(".taskSub, .taskTypeImg").each(function(){
			                	var title = $(this).attr('title').replaceAll("CHR(41)"," ");
			                	$(this).attr('title',title);
							 });
							 var isiPad = navigator.userAgent.match(/iPad/i) != null;
					         if(!isiPad)
					         	$("#assignedtaskdatalist").mCustomScrollbar("update");
					          else
					            $('#assignedtaskheaders').css('width','100%');	
					         $('#assignedTaskImg').css('opacity','1');	    
							 parent.$("#loadingBar").hide();
							 parent.timerControl("");
						 }
					   }
			   		});
			    }}
	        },
	        onTotalScrollOffset:100
		  }
	  });	
	  $('div#AssignedtaskList .mCSB_container').css('margin-right','15px');
	  $('#assignedtaskdatalist div div.mCSB_container').css('width','98.2%');
	  $('#assignedtaskdatalist div div.mCSB_scrollTools').css('margin-left','0.5%');
}
var historyScroll=true;
function historyTaskListScroll(startdatetext,enddatetext,optionval,taskSortVal,taskSortType) {
	
	$("#historytaskdatalist").mCustomScrollbar({
		  scrollButtons:{
			  enable:true
		  },callbacks:{
	        onTotalScroll:function(){
	        if(taskSortType=="All" || taskSortType=="" || taskSortType=="all"){
	             parent.$("#loadingBar").show();
	  	    	parent.timerControl("start");
			    limit = limit+20;
			    if(historyScroll == false) {
			    	parent.$("#loadingBar").hide();
					parent.timerControl("");
			    }
			    else {
			    	$.ajax({ 
				       url: Path+'/calenderAction.do',
				       type:"POST",
					   data:{act:'FetchHistoryListViewData', startdate:startdatetext, enddate:enddatetext, optionval:optionval,limit:limit,taskSortVal:taskSortVal,taskSortType:taskSortType,searchWord:searchWord},
					   error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
								}, 
					   success:function(result){
					  	parent.checkSessionTimeOut(result);
					   	 var data = result.split("~historyData~");
					   	 if(data[1] == '<div id="HistoryList" class="listViewDatas"></div>'){
					   	 	historyScroll = false;
				   			parent.$("#loadingBar").hide();
							parent.timerControl("");
						 }
						 else {
						 	 historyScroll=true;
						 	 $("#HistoryList").append(data[1]);
						     $(".taskSub, .taskTypeImg").each(function(){
			                	var title = $(this).attr('title').replaceAll("CHR(41)"," ");
			                	$(this).attr('title',title);
							 });
						     var isiPad = navigator.userAgent.match(/iPad/i) != null;
				        	 if(!isiPad)
				            	$("#historytaskdatalist").mCustomScrollbar("update");
				             else
					            $('#historytaskheaders').css('width','100%');	
					            $('#historyTaskImg').css('opacity','1');
				             parent.$("#loadingBar").hide();
							 parent.timerControl("");
				            }	
				          }   		
				   });
			    }}
	        },
	        onTotalScrollOffset:100
		  }
	  });	
	  $('div#HistoryList .mCSB_container').css('margin-right','15px');
	  $('#historytaskdatalist div div.mCSB_container').css('width','98.2%');
	  $('#historytaskdatalist div div.mCSB_scrollTools').css('margin-left','0.5%');
}


 function showHistoryList(){
 	$('#taskCreate').hide();
 	$('#colabusTaskImg,#googleTaskImg').hide();
 	$('#historyFilter').show();
 	$("#myTaskChart").hide();
 	$("#assignedTaskChart").hide();
 	$('#filterDiv').hide();                //$('#selectDropDownDiv').hide();//$('#selectTaskType').hide();
 	$('#searchBox').val('');
 	$('#calendarSortDiv').slideUp();
 	$('#defaultSort').attr('selected','selected');
 	$('#defaultTaskSort').attr('selected','selected');
 	var height = $(window).height();
	var sortDivHeight=$("#headerTopicDiv").height();
	var CalTabDivHeight=$("#CalenderListViewTabDiv").height();
	var CalHeaderDivHeight=$("#historyheaders").height();
	var dataHeight=height-sortDivHeight-CalTabDivHeight-CalHeaderDivHeight-45;
	$("#mytaskdatalist").css('height',dataHeight);
	$("#assignedtaskdatalist").css('height',dataHeight);
	$("#historytaskdatalist").css('height',dataHeight);
    $("#MyTasklistView").hide();
    $("#AssignedlistView").hide();  
    $("#HistorylistView").show(); 
    $("#mytaskdatalist").hide();
    $("#assignedtaskdatalist").hide();
    $("#historytaskdatalist").empty();
    $("#historytaskdatalist").show();               //$("#HistoryList").show();
    $("#mytaskTab").css('color','#A7A7A7');
    $("#assignedTab").css('color','#A7A7A7');
    $("#historyTab").css('color','#FFFFFF');
    $('#myTaskImg').css('opacity','0.6');
    $('#assignedTaskImg').css('opacity','0.6');
    $("#breadCrumName").html('&nbsp;> '+getValues(Labels,'HISTORY'));
    $('#calendarSortDiv').css('right','80px');
 	$('#defaultSort').attr('selected','selected');
 	$('#defaultTaskSort').attr('selected','selected');
    $('#calendarSortDiv').css('margin-top','-19px');
 	$('#WsarrowDiv').css('margin-top','14px');
    parent.$("#loadingBar").show();
    parent.timerControl("start");
    limit = 0;
    historyScroll = true;
     $.ajax({ 
	       url: Path+'/calenderAction.do',
	       type:"POST",
		   data:{act:'FetchHistoryListViewData',limit:limit},
		   error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
		   success:function(result){
		   parent.checkSessionTimeOut(result);
		   var data = result.split("~historyData~");
	       $("#historytaskdatalist").html(data[1]);
	       $(".taskSub, .taskTypeImg, .myTask").each(function(){
	       		var title = $(this).attr('title').replaceAll("CHR(41)"," ");
				$(this).attr('title',title);
		   });
	       var isiPad = navigator.userAgent.match(/iPad/i) != null;
           if(!isiPad)
            	historyTaskListScroll("","","","","");
           else {$("#historytaskdatalist").css("margin-top","-15px");
            	$('#historyheaders').css('width','100%');
	            $("#historytaskdatalist").scroll(function(){
     				if($("#historytaskdatalist").scrollTop() >= ($("#HistoryList").height() - $("#historytaskdatalist").height()) && myTaskScroll == true)
                    {	
                    	historyIpadScroll("","","","","");
                    }
                });    
            }	if($('.historyTaskPageClass').length==0){$('#HistoryList').append('<div id="innerDiv8" align="center" style="color:#ffffff;margin-top:20px;font-size:15px;font-family:icon;">No Task Available</div>');}		
	         initFilterCal();
	  		 initFilterCal1();     
	  		 $('#historyTaskImg').css('opacity','1');
	  		 parent.$("#loadingBar").hide();
	  		 parent.timerControl("");
			 sortTaskType = 'history';
			 $('#searchBox').val('');
			 $('#noteSearch').attr('src',Path+'/images/Idea/search.png').css('top','20px');	
			}
	   });
 }

 
function showAssignedTaskList(){
	moreValue=='assignedTasks';
	$('#taskCreate').show();
 	$('#colabusTaskImg,#googleTaskImg').hide();
 	$('#historyFilter').hide();
 	$('#sortInWs').show();
 	$('#searchBox').val('');
 	$('#selectDropDownDiv').show();
 	$('#selectTaskType').show();
 	$('#calendarSortDiv').slideUp();
 	$('#defaultSort').attr('selected','selected');
 	$('#defaultTaskSort').attr('selected','selected');
 	var height = $(window).height();
	var sortDivHeight=$("#headerTopicDiv").height();
	var CalTabDivHeight=$("#CalenderListViewTabDiv").height();
	var CalHeaderDivHeight=$("#assignedtaskheaders").height();
	var dataHeight=height-sortDivHeight-CalTabDivHeight-CalHeaderDivHeight-45;
	$("#mytaskdatalist").css('height',dataHeight);
	$("#assignedtaskdatalist").css('height',dataHeight);
	$("#historytaskdatalist").css('height',dataHeight-32);
    $("#MyTasklistView").hide();
    $("#HistorylistView").hide(); 
    $("#AssignedlistView").show();
    $("#mytaskdatalist").hide();
    $("#assignedtaskdatalist").show();
    $("#assignedtaskdatalist").empty();
    $("#mytaskdatalist").html('');
    $("#historytaskdatalist").hide();
    $("#myTaskChart").hide();
    $("#assignedTaskChart").show();
    $("#mytaskTab").css('color','#A7A7A7');
    $("#assignedTab").css('color','#FFFFFF');
    $("#historyTab").css('color','#A7A7A7');
    $('#historyTaskImg').css('opacity','0.6');
    $('#myTaskImg').css('opacity','0.6');
    $("#breadCrumName").html('&nbsp;> '+getValues(Labels,'ASSIGNED_TASKS'));
    parent.$("#loadingBar").show();
    parent.timerControl("start");
    limit = 0;    
    assignedScroll = true;
		$.ajax({ 
	       url: Path+'/calenderAction.do',
	       type:"POST",
		   data:{act:'FetchAssignedTaskListViewData',limit:0},
		   error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
		   success:function(result){
		   parent.checkSessionTimeOut(result);
		   moreValue='assignedTasks';
		     var data = result.split("~assginTask~");
		     $("#assignedtaskdatalist").html(data[1]);
		     $(".taskSub, .taskTypeImg").each(function(){
				var title = $(this).attr('title').replaceAll("CHR(41)"," ");
				$(this).attr('title',title);
			 });
			 var isiPad = navigator.userAgent.match(/iPad/i) != null;
	         if(!isiPad)
	         	assignedTaskListScroll("","");
	          else {
	            $('#assignedtaskheaders').css('width','100%');
	            $("#assignedtaskdatalist").scroll(function(){
     				if($("#assignedtaskdatalist").scrollTop() >= ($("#AssignedtaskList").height() - $("#assignedtaskdatalist").height()) && myTaskScroll == true)
                    {	
                    	assignedIpadScroll("","");
                    }
                }); 
	          }if($('.task').length==0){$('#AssignedtaskList').append('<div id="innerDiv7" align="center" style="color:#ffffff;margin-top:20px;font-size:15px;font-family:icon;">No Task Available</div>');}  	
	         $('#assignedTaskImg').css('opacity','1');	    
			 parent.$("#loadingBar").hide();
			 parent.timerControl("");
			 sortTaskType = 'assignedTasks';
		     $('#searchBox').val('');
			 $('#noteSearch').attr('src',Path+'/images/Idea/search.png').css('top','20px');
			 }
	   });
 }


 function showMyTaskList(){
 	moreValue='myTasks';
    $('#taskCreate').show();
 	$('#googleTaskImg').show();
	$('#colabusTaskImg').hide();
	$("#myTaskChart").show();
	$('#assignedTaskChart').hide();
	$('#myTaskSection').attr('onClick','showMyTaskList()');
 	$('#historyFilter').hide();
 	$('#sortInWs').show();
 	$('#searchBox').val('');
 	$('#selectDropDownDiv').show();
 	$('#selectTaskType').show();
 	$('#calendarSortDiv').slideUp().css('right','80px');
 	$('#defaultSort').attr('selected','selected');
 	$('#defaultTaskSort').attr('selected','selected');
    $('#calendarSortDiv').css('margin-top','-19px');
 	$('#WsarrowDiv').css('margin-top','14px');
 	var height = $(window).height();
	var sortDivHeight=$("#headerTopicDiv").height();
	var CalTabDivHeight=$("#CalenderListViewTabDiv").height();
	var CalHeaderDivHeight=$("#mytaskheaders").height();
	var dataHeight=height-sortDivHeight-CalTabDivHeight-CalHeaderDivHeight-45;
	$("#mytaskdatalist").css('height',dataHeight);
	$("#assignedtaskdatalist").css('height',dataHeight);
	$("#historytaskdatalist").css('height',dataHeight-32);
	$("#MyTasklistView").show();
    $("#AssignedlistView").hide();
    $("#HistorylistView").hide();  
    $("#assignedtaskdatalist").hide();
    $("#historytaskdatalist").hide();
    $("#mytaskdatalist").show();
    $("#MytaskList").show();
    $("#mytaskTab").css('color','#FFFFFF');
    $("#assignedTab").css('color','#A7A7A7');
    $("#historyTab").css('color','#A7A7A7');
    $('#historyTaskImg').css('opacity','0.6');
    $('#assignedTaskImg').css('opacity','0.6');
    $("#breadCrumName").html('&nbsp;> '+getValues(Labels,'MY_TASKS'));
    $("#mytaskdatalist").html('');
    $("#assignedtaskdatalist").html('');
    parent.$("#loadingBar").show();
    parent.timerControl("start");
    if($('#mytaskdatalist').attr('class')) {
    	$('#mytaskdatalist').removeClass('taskDataList');
    }
     limit = 0; 
     myTaskScroll = true;
     $.ajax({ 
	       url: Path+'/calenderAction.do',
	       type:"POST",
		   data:{act:'FetchMyTaskListViewData',limit:0},
		   beforeSend: function (jqXHR, settings) {
		        xhrPool.push(jqXHR);
		    },
		   error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
		   success:function(result){
		   parent.checkSessionTimeOut(result);
		   	    $("#mytaskdatalist").html(result);
		   	    $(".taskSub, .taskTypeImg").each(function(){
					var title = $(this).attr('title').replaceAll("CHR(41)"," ");
					$(this).attr('title',title);
				});
				var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        	if(!isiPad)
	            	myTaskListScroll("","",searchWord);
	            else {
	            	$('#mytaskheaders').css('width','100%');	
	            	$("#mytaskdatalist").scroll(function(){
	     				if($("#mytaskdatalist").scrollTop() >= ($("#MytaskList").height() - $("#mytaskdatalist").height()) && myTaskScroll == true)
	                    {	
	                    	myTaskIpadScroll("","",searchWord);
	                    }
	                }); 
	            }if($('[id^=myTask_]').length==0){$('#MytaskList').append('<div id="innerDiv6" align="center" style="color:#ffffff;margin-top:20px;font-size:15px;font-family:icon;">No Task Available</div>');}
				$('#myTaskImg').css('opacity','1');	
				parent.$("#loadingBar").hide();
				parent.timerControl("");
				sortTaskType = 'myTasks';
				$('#searchBox').val('');
			    $('#noteSearch').attr('src',Path+'/images/Idea/search.png').css('top','20px');
			    moreValue='myTasks';
		    }
     });
   }

 function showHistoryFilter(){
 	var sortDivHeight=$("#headerTopicDiv").height();
	var CalTabDivHeight=$("#CalenderListViewTabDiv").height();
	var CalHeaderDivHeight=$("#mytaskheaders").height();
	var dataHeight=height-sortDivHeight-CalTabDivHeight-CalHeaderDivHeight-45;
	$('#filterStartDate,#filterEndDate').val('');
	if($('#filterDiv').is(':hidden')){
 		$('#filterDiv').show();
 		$("#historytaskdatalist").css('height',dataHeight-32);
 	}
 	else {
 		$('#filterDiv').hide();
 		$("#historytaskdatalist").css('height',dataHeight);
 	}
 }


 var newStartDate='';
 var newEndDate='';
  function setSnEdates(sprintId){
    var sDate=$('#startDateSprint').val().trim();
   var eDate=$('#endDateSprint').val().trim();
   newStartDate=$('div#ddList3').children('ul').find('li#'+sprintId).children('input.sprintStartDate').val();
   newEndDate=$('div#ddList3').children('ul').find('li#'+sprintId).children('input.sprintEndDate').val();
   if(sDate=='' || eDate==''){
      $('#startDateSprint').val(newStartDate);
      $('#endDateSprint').val(newEndDate);
      $('#taskSprint').val(sprintId);
   }else{
   		parent.confirmReset(getValues(customalertData,"Alert_Reset_Date"),"reset","confirmSprintName","cancelSprintEdit");
   }
 }
 function confirmSprintName(){
 	$('#startDateSprint').val(newStartDate);
    $('#endDateSprint').val(newEndDate);
    $('#taskSprint').val(sprintId);
 }
 function cancelSprintEdit(){
 	$('div#ddList3').children('ul').find('li').hide();
    var sprintGroup=$('#taskSprintGroup').val();
    var sprintNameId=$('#taskSprint').val();
    var sprintName=$('div#ddList3').children('ul').find('li#'+sprintNameId).children('a').text();
    $('div#ddList3').children('span').text(sprintName);
    $('div#ddList4').children('ul').find('li#'+sprintGroup).show();
    $('div#ddList3').children('ul').find('li.sprintLi_'+sprintGroup).show();
 }          

  function confirmTaskHourDivClose(){
  	 prevSDate = $('#startDateSprint').val();
  	 prevEDate = $('#endDateSprint').val();
  	 $('input#taskTotalEhour').val(confirmId); 
  	 hideTaskHourDiv();
 }


 function clearTaskDate(){
   $('.estimatedHourDiv').each(function() {
		$(this).children('div').children('input').val("");
	});
 }
 function checkNumerals(obj){
                var intRegex = /^\d+$/;
		        var floatRegex = /^((\d+(\.\d *)?)|((\d*\.)?\d+))$/;
				var str = $(obj).val().trim();
		        if(str!=''){
			        if(intRegex.test(str) || floatRegex.test(str)) {
			           return true;
				    }else{	
		   		       parent.alertFun(getValues(companyAlerts,"Alert_InvalidNumber"),'warning');
		   		       $(obj).css('border','1px solid red');
		   		       return null;
			   		}
			  }else{
			     return null;
			  }
  }


	function confirmDate(){
		 if(endId == 'endAfter') {
		 	if($('#noOfOccurences').val() > 0) {
		    	recEnd = $('#noOfOccurences').val()+' occurence(s)';
		    }		
		   	else {
		   		$('#noOfOccurences').css('border','1px solid red');
		   		return null;
		   	}	
	    }	
	    if(endId == 'endDate') {
	    	if($('#recEnd').val()) {
	   			var date = $('#recEnd').val().split('-');
	   			recEnd=date[2]+'-'+date[0]+'-'+date[1];
	   		}
		   	else {
		   		$('#recEnd').css('border','1px solid red');
		   		return null;
		   	}	
	    }
	    if(endId == 'noEnd') {
	    	recEnd= 'No End';
	    }
	   $('#recurrencePopupDiv').hide();
	}

	var documentId = '';
	function downloadFile(docId){	/* Document DownLoad*/
		documentId = docId;
		parent.confirmFun(getValues(customalertData,"Alert_Download_Doc"),"close","downloadDocument");
	}
	function downloadDocument(){
		var url="";
		if(parId == 'myDocument' || parId == 'assignedDocument')
			url = Path+"/repositoryAction.do?action=downloadfile&docId="+documentId ;
		else
			url = Path+"/workspaceAction.do?act=downloadfile&docId="+documentId+"&projId="+projId;
		window.open(url);
	}

   		

	function switchToColabusTasks(){
		$('#colabusTaskImg').hide();
		$('#googleTaskImg').show();	
		$('#assignedTaskContainer').show();
		$('#calendarSortDiv').css('height','auto');
		$('#historyContainer').show();
		$('#myTaskSection').attr('onClick','showMyTaskList()');
		parent.menuFrame.location.href = Path+'/colAuth?pAct=calListView'
	}
    
    //For upload document for task
     /*function uploadDocumentPopUp(){
        $("#uploadTaskLevelDoc").show();
        var docTaskType=$("#doctaskType").val();
            $('#WsUploadOptns').slideToggle("slow");
           $("#docTaskUpload").show();
           $("#docTaskContentSubDiv").css('height','260px');
        
          if(docTaskType == "myTasks"){
             $("#docTaskUpload").hide();
             $("#docTaskContentSubDiv").css('height','300px');
           }
         $("#overlay").show();
        if(tflag == true){
           fetchDocdata('default');
        }
     }*/
     
  function uploadDocumentPopUp(){
        $("#uploadTaskLevelDoc").show();
        var docTaskType=$("#doctaskType").val();
        if(docTaskType == "assignedTasks" || docTaskType =="" || docTaskType == "assignedSprint" || docTaskType == "assignedIdea" || docTaskType == "event" || docTaskType == "assignedEvent"){
            $('#WsUploadOptns').slideToggle("slow");
        }
        if(docTaskType == "assignedTasks" || docTaskType == "assignedSprint" || docTaskType == "assignedIdea" || docTaskType == "event" || docTaskType == "assignedEvent"){
           $("#docTaskUpload").show();
           $("#docTaskContentSubDiv").css('height','273px');
        }else{
           if(docTaskType == "myTasks" || docTaskType == "mySprint" || docTaskType == "myIdea" || docTaskType == "myEvent" || docTaskType == "myEvents"){
             $("#docTaskUpload").hide();
             $("#docTaskContentSubDiv").css('height','310px');
           }
        }
        $("#overlay").show();
        $("#TaskLevellinkSection").hide();
        if(tflag == true){
           fetchDocdata('default');
        }
      }
       function fetchDocdata(type){
        parent.$("#loadingBar").show();
		parent.timerControl("start");
        var docTaskId=$("#uploadDocTaskId").val();	
	    var docTaskType=$("#doctaskType").val();	
	    var docTaskprjId=$("#taskLevelprojectId").val();
	    if(docTaskType == "event"){
	      menuType="event";
	    }else{
	      menuType="cal";
	    }
        $.ajax({ 
	       url: Path+'/calenderAction.do',
	       type:"POST",
		   data:{act:'FetchTaskLevelDocuments',docTaskId:docTaskId,docTaskType:docTaskType,type:type,docTaskprjId:docTaskprjId,menuType:menuType},
		   error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
		   success:function(result){
		       parent.checkSessionTimeOut(result);
		       if(result == ""){
		          $("#docTaskContentSubDiv").html("<div align=\"center\" style=\"color:#000000;font-size:14px\">No documents found.</div>");
		       }else{
		          $("#docTaskContentSubDiv").html(result);
		       }
		       
		       tflag = false;
		       scrollBar('docTaskContentSubDiv');
		       if($("#docTaskContentSubDiv").find('div.mCSB_scrollTools').css('display') == 'block'){ 
		          $("#taskLevelFldHeader").css("width","98%");
		          $('#docTaskContentSubDiv .mCSB_container').css('margin-right','17px'); 
		          $(".hideOptionDiv").css('margin-right','25px');
		       }else{
		          $("#taskLevelFldHeader").css("width","99%");
		          $('#docTaskContentSubDiv .mCSB_container').css('margin-right','7px');
		          $(".hideOptionDiv").css('margin-right','40px'); 
		       }
		       if($('div#docTaskContentSubDiv').find('.mCSB_container').hasClass('mCS_no_scrollbar')){
			    		    $('div#docTaskContentSubDiv').children().children('.mCSB_container').css("position","static");
	    	     			//$('div#galleryAlbumContDiv').children().children('.mCSB_container').css("height","100%");
	    	      			$("div#docTaskContentSubDiv").mCustomScrollbar("update"); 
	    				}
		       parent.$("#loadingBar").hide();
			   parent.timerControl("");
		   }
		   });
     }
     function TaskDocUpload(){
        $('#WsUploadOptns').slideToggle("slow");
     }
     
     var glbDocId="";
     var glbTaskId="";
     var glbdelType="";
     function deleteTaskLevelDocument(docId,taskId,delType){
        glbDocId=docId;
        glbTaskId=taskId;
        glbdelType=delType;
        if(delType == 'link'){
          parent.confirmFun(getValues(companyAlerts,"Alert_DoDelLink"),"delete","deleteDocConfirm");
        }else{
           parent.confirmFun(getValues(companyAlerts,"Alert_DelFolDoc"),"delete","deleteDocConfirm");
        }
     }
     
     function deleteDocConfirm(){
        var docTaskType=$("#doctaskType").val();
         parent.$("#loadingBar").show();
		 parent.timerControl("start");
		 if(glbdelType == 'link'){
		    $("#linkLevel_"+glbDocId).remove();
		 }else{
            $("#doc_"+glbDocId).remove();
         }
         $.ajax({ 
	       url: Path+'/calenderAction.do',
	       type:"POST",
		   data:{act:'DeleteTaskLevelDocuments',docId:glbDocId,docTaskType:docTaskType,taskId:glbTaskId,glbdelType:glbdelType},
		   error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
		   success:function(result){
		       parent.checkSessionTimeOut(result);
		       if(result == "success"){
		           if(glbdelType == 'link'){
			        parent.alertFun(getValues(companyAlerts,"Alert_LinkDel"),'warning');
			       }else{
			        parent.alertFun(getValues(companyAlerts,"Alert_DocDel"),'warning');
			       }
		       }
		       $("div#docTaskContentSubDiv").mCustomScrollbar("update"); 
		       if($("#docTaskContentSubDiv").find('div.mCSB_scrollTools').css('display') == 'block'){ 
		          $("#taskLevelFldHeader").css("width","98%");
		          $('#docTaskContentSubDiv .mCSB_container').css('margin-right','17px'); 
		          $(".hideOptionDiv").css('margin-right','25px');
		       }else{
		          $("#taskLevelFldHeader").css("width","99%");
		          $('#docTaskContentSubDiv .mCSB_container').css('margin-right','7px'); 
		          $(".hideOptionDiv").css('margin-right','40px');
		       }
		       parent.$("#loadingBar").hide();
			   parent.timerControl("");
		   }
		   });
     }
     

//Added method for creation of task in list view.
	/*function createTaskInlist(){					
				initCal();
				var date = new Date();
				var d = date.getDate();
				var m = date.getMonth();
				var y = date.getFullYear();	    
				$('.Create').text(getValues(Labels,'Save'));
				$('.Create').attr('title',getValues(Labels,'Save'));
				var currentDate = new Date();
				var day = currentDate.getDate();			
				var month = currentDate.getMonth() + 1;				
				var year = currentDate.getFullYear();
				var hour = currentDate.getHours();
				var minute = currentDate.getMinutes();
				var stTime = hour+':'+minute;
				update = 'no';
				getAllUsers();
				$('.calInstructions').text($("#taskMsg").val());			
				$('#startDateDiv').val('');
				if(userType == 'web_standard')
					$('#selectTask > .Work_Flow').hide();
				else
					$('#selectTask > .Work_Flow').show();
	   			$("#eventPopup").show();
	   			$('#eventContainer').show();
	   			$('#actualHourContentDiv').show();
	   			$('#projectUsers').show();
	   			$('.estmHour').show();
	   			timePicker();
	   			$('#pariticipateDiv').show();			
				stDate = $.fullCalendar.formatDate(date, 'MM-dd-yyyy');
				var sendDate = $.fullCalendar.formatDate(date, 'yyyy/MM/dd');
				sendDate = new Date(Date.parse(sendDate.replace(/-/g,' ')))
				today = new Date();
				today.setHours(0,0,0,0);
				if (sendDate < today) {
					stDate = $.fullCalendar.formatDate(today, 'MM-dd-yyyy');
				} 				
				$('#startDateDiv').val(stDate);		
				$('#endDateDiv').val(stDate);
				$('#createEvent').removeClass().addClass('createEvent');
	   			$('#wFTaskDateSave').css('display','none');
	   			$('#taskDateSave').css('display','block');
	   			$('.createEvent').attr('onclick','addTasks();');
				$('.transparent').show();
				parent.$("#transparentDiv").show();
				$('select#project').val(getValues(Labels,"All_Users"));
				dayObj = $(this);
				var el = document.getElementById('createButton');
			    el.scrollIntoView(true);
			    initCal();
			    getAllProject();
			    if(height < 550){
				    var isiPad = navigator.userAgent.match(/iPad/i) != null;
		    		if(!isiPad)	{
						scrollBar('eventPopup');
					}	
				}
	}
	
	
*/
	
	
	
	
	
	
	
	