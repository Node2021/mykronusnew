/*#    Name:	constants.js
# Purpose:	Stores the constance configuration for the application

# Version:	1.0 */

//#physical path of project
//com.rs.root.path="D://StridusWorkspace//ColabusV3//web"

//#Path to the repository
//com.rs.repository.path="D:\ColabusV3Repo"
//#Path to the repository lighttpd server
//#com.rs.lighttpd.path=http://192.168.0.90:1937//
//com.rs.lighttpd.path="https://newtest.colabus.com:1933//"

//#Domain Name(Server) for of project ( log-bug.com )
//com.rs.domain.name=log-bug.com

//#Path to the Reports and Dashboard 
//com.rs.report.path="D://Colabusv3//dashboard//"

//#range for the documents to store in folder
//com.rs.documents.range=100000

//com.rs.xml.path="D://Colabusv3//"

//#mail server name
//com.smtp.server.name="mail.colabus.com"

//#mail server name port
//com.smtp.server.port=2525

//#mail server user default email
//com.smtp.server.default.email="info@colabus.com"

//#mail server default email pwd
//com.smtp.server.default.email.pwd="Str1dUS@2134"

//#email client config
//com.scheduler.lighttpd="http://208.109.219.25:1935//emailAttachments//"

//#CSV file path
//com.csv.file.path="D://csvPath//"

module.exports = {
    rootpath : "D://StridusWorkspace//ColabusV3//web",
    repositorypath : "D:\ColabusV3Repo",
    lighttpdpath : "https://newtest.colabus.com:1933//"
    /*reportpath : "D://Colabusv3//dashboard//",
    documentsrange=100000,
    xmlpath="D://Colabusv3//",
    servername="mail.colabus.com",
    serverport=2525,
    serverdefaultemail="info@colabus.com",
    serverdefaultemailpwd="Str1dUS@2134",
    schedulerlighttpd="http://208.109.219.25:1935//emailAttachments//",
    csvfilepath="D://csvPath//" */

};