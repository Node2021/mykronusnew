
var tflag=false;
function viewTask(task_id,obj,divId) {		/* Edit Assigned Task */
	update='yes';			
	var id = obj.id;$("#participantMail").css("margin-top","0px");
	myTaskId=task_id;
	parId = divId;
	parent.$('#loadingBar').show();
	parent.timerControl("start");	
	$("#uploadDocTaskId").val(task_id);	
	$("#doctaskType").val(divId);
	if(divId == 'assignedTasks' || divId == 'myTasks' || divId == 'mySprint'){
	$("#moreButton").show();

	   tflag=true;
	   $("#taskDocCreateImg").show();
	   $(".uploadTaskIcon").show();
	   $('#createOrCancel').css('width','333x');
	//   $('#createOrCancel').css('width','200px');
	}else{
		$("#dependencyTask").hide();
	   $("#taskDocCreateImg").hide();
	   $(".uploadTaskIcon").hide();
	 //  $('#createOrCancel').css('width','230px');
	   $('#createOrCancel').css('width','180px');
	}
	
	if(divId == 'assignedTasks' || divId == 'assignedDocument' || divId == 'assignedWorkspaceDocument' || divId == 'assignedWorkFlow' || divId=='assignedWorkflow' || divId == 'assignedEvent' || divId == 'assignedIdea' || divId == 'assignedEmail') {
		eId = myTaskId;
		$.ajax({ 
           url: Path+'/calenderAction.do',
       	   type:"POST",
	       data:{act:'fetchAssignedTaskDetails',taskId:task_id, taskType:divId},
	       error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
	       success:function(result){	
	            parent.checkSessionTimeOut(result);
	            $("#eventPopup").show();
	            
	            $("#taskPopupSection").show();
				$('.transparent').show();
				parent.$("#transparentDiv").show();
				if(height >= 550)
					$('#eventPopup').css('height','550px');
				else {
					$('#eventPopup').css({'width':'850px','height':height+'px'});
					$('.eventPopup').css({'margin-top':'','margin-top':'-'+height/2+'px'});
					scrollBar('eventPopup');
				}
				if(parId != 'assignedWorkflow')	
					$('.wFTaskDateSave').hide();
				$("#unassgntaskDiv").css('display','none');
				$("#assgntaskDiv").css('display','block');
			//	$('.To').css("margin-left","113px");
	       		initCal();
				timePicker();
				$('#deleteTask').show().attr("onClick",'deleteEvent('+eId+',"'+divId+'")');
				$('#dd5 span').text(result.split('^&*')[0]);
	       		$('#eventDescription').val(result.split('^&*')[1]);
	       		$('#eventReminder').val(result.split('^&*')[2]);
	       		$('#remindThrough').val(result.split('^&*')[3]);
	       		var title = result.split('^&*')[4];
	       		var sDate = result.split('^&*')[5];
	       		tmpSDate = sDate;
	       		startDate = sDate;
	       		
	       		var eDate = result.split('^&*')[6];
	       		tmpEDate = eDate;
	       		endDate = eDate;
	       		prevDateStartSave=sDate;
 				prevDateEndSave=eDate;
	       		var sTime = result.split('^&*')[7];
	       		var eTime = result.split('^&*')[8];
	       		var projName = result.split('^&*')[9];
	       		var taskImages= result.split('^&*')[10];
	       		var place = result.split('^&*')[11];
	       		recType=result.split('^&*')[12];
	       		recPatternValue=result.split('^&*')[13];
	       		recEnd=result.split('^&*')[14];
	       		projId=result.split('^&*')[15];	
	       		$("#taskLevelprojectId").val(projId);			
	       		tHour=result.split('^&*')[16];
	       		aHour=result.split('^&*')[17];
	       		statImg=result.split('^&*')[18]; 
	       		var docId=result.split('^&*')[19];
	       		tMinute=result.split('^&*')[20];
	       		aMinute=result.split('^&*')[21];	
	       		 doc_projId=	result.split('^&*')[22];	
	       		 doc_projName=result.split('^&*')[23];	
	       		 var docIconStatus=result.split('^&*')[24];
	       		serverHour=tHour;
	       		prevHourVal=tHour;
	       		prevMinuteVal=tMinute;
	       		$('.actualHour').show();
	       		$('#actualWorkHour').val(aHour);
	       		$('#actualWorkMinute').val(aMinute);
	       		
	       		$("#totalMinute").val(tMinute);
	       		
	       		if(tHour==aHour && aMinute>tMinute){		
	       			$('#actualWorkHour').css('color','#FF0000');
	       			$('#actualWorkMinute').css('color','#FF0000');
	       			}
	       		else if(aHour > tHour ){					
	       			$('#actualWorkHour').css('color','#FF0000');
	       			$('#actualWorkMinute').css('color','#FF0000');
	       			}
	       		else{
	       			$('#actualWorkHour').css('color','#848484');
	       			$('#actualWorkMinute').css('color','#848484');
	       		}
	       		
	       		$('.Create').text(getValues(Labels,'Save'));
	       		$('.Create').attr('title',getValues(Labels,'Save'));
				$('#createEvent').removeClass();
	       		$('.createEvent').attr('onclick','updateAssignedTasks("'+divId+'");');
	       		$('#eventContainer').css('display','block');
				$('#projectUsers').css('display','block');
				$('#actHour').css('display','block');
				$('select#userProject').val(getValues(Labels,'All_Users'));
				//alert(divId);
				if(divId == 'assignedTasks' || divId == 'assignedIdea' || divId == 'assignedEvent'){
				    if(docIconStatus==""){
				       $("#docIconNameLevel").hide();
				       $("#eventName").css("width",'412px');
				    }else{
				       $("#eventName").css("width",'390px');
				       $("#eventName").css("float",'left');
				       $("#docIconNameLevel").show();
				    }
				    
				}
				if(divId == 'assignedDocument' || divId == 'assignedWorkspaceDocument') {
	       		    $("#pariticipateDiv").css('display','none');
		            $('#projectSection').css('display','none');
		            $('#participantEmail').css({'height':'173px','width':'98%'});
		           	$('#prjSection').css('display','none');
	       			$('#taskSection').css('display','block');
	       			$('#tskSection').css('display','block');
	       			$("#docType").css("overflow","");
	       			if(divId == 'assignedDocument'){
	       				$('select#userProject').val(getValues(Labels,'All_Users'));
	       				getProjectUsers(document.getElementById("userProject"));
		       			$("#docDownload").show();
		       			$("#docDownload").attr('onClick','downloadFile("'+docId+'")');
		       			$("#eventName").css('width','390px');
		       			$("#docDownload").attr('title','Download document');
		       			$("#docDownload").css({'margin-top':'11px','margin-left':'2px','cursor':'pointer'});
		       		}
		       		if(divId == 'assignedWorkspaceDocument'){
			       		if(doc_projName != '-' && doc_projName!='') {
							$('select#userProject').val(doc_projName);
							document.getElementById("userProject").value=doc_projName;
							getProjectUsers(document.getElementById("userProject"));
						}	
						if(doc_projName == getValues(Labels,'Select') || doc_projName == ''){
							$('select#userProject').val(getValues(Labels,'All_Users'));
							getProjectUsers(document.getElementById("userProject"));
						}
			       	}
	       			if(projName == '-' || projName == '') {
	       				$('#docType span').text(getValues(Labels,'Select'));
	       				$('select#userProject').val(getValues(Labels,'All_Users'));
	       			}
	       			else{ 	
	       					$('#docType span').text(projName);
	       			}
	       			
	       			var remainder=result.split('^&*')[2];
	       			var remainderType=result.split('^&*')[3];
		       		if(divId == 'assignedWorkspaceDocument' ||divId=="assignedDocument"){
				    	if(remainder=="minutes"){
				    		$('#eventReminder').val("30 minutes");
				    	}
				    	else if(remainder=="hour" || remainder=="Hour"){
				    	   $('#eventReminder').val("1 hour");
				    	}	
				        else if(remainder=="day" || remainder=="Day"){
				    	  $('#eventReminder').val("daily");
				    	}
				    	else if(remainder=="week" || remainder=="Week"){
				    	  $('#eventReminder').val("weekly");
				    	}
				    	else if(remainder=="Month" || remainder=="month"){			
				    	  $('#eventReminder').val("monthly");
				    	}
				    	else if(remainder=="year" || remainder=="Year"){
				    	  $('#eventReminder').val("yearly");
				    	}
				    	if(remainderType=="email" || remainderType=="Email"){
				    	  $('#remindThrough').val("by email");
				    	}	
				    }		
		       	
	       					
	       		}
	       		
	       		else if(divId == 'assignedTasks' || divId == 'assignedIdea' || divId == 'assignedWorkflow' ) {
		       		$("#pariticipateDiv").css('display','none');
			        $("#addParticipantTasks").css('height','173px');
			        $('#participantEmail').css('height','173px');
		        	$('.selectPlace').hide();
					$('#emailDiv').css('display','none');
		       		if(projName != '-' && projName!='') {
						$('select#project').val(projName);
						$('select#userProject').val(projName);
						getProjectUsers(document.getElementById("userProject"));
						document.getElementById("project").value=document.getElementById("userProject").value;
					}	
					if(projName == getValues(Labels,'Select') || projName == ''){
						$('select#project').val(getValues(Labels,'Select'));
						$('select#userProject').val(getValues(Labels,'All_Users'));
						getProjectUsers(document.getElementById("userProject"));
					}
					 
				}	
				else if(divId == 'assignedEvent') {
					$('#userListContainer').html('');$('#userListContainer').append('<div id="loadingB" align="center" style="margin-top:230px;overflow:hidden">loading...</div>');
					$('.estmHour').hide();
					$('#actualHourContentDiv').hide();
					initEventCal();
					$('#emailDiv').show();
			        $('.taskDate').hide();
			        $('.eventDateDiv').show();
			        $('#pariticipateDiv').show();
			        $('#taskHeaderSec').css('height','42px');
			        $('.calInstructions').hide();
			        $('#addParticipantTasks').css('height','176px');
			        $('#participantEmail').css('height','50px');
					$('#pariticipateDiv').css('padding-top','20px');
					$('#taskDetails').css('height','277px');
					$('.selectPlace').show();
					$('#pariticipateDiv').parent('div').css('display','block');
					$('#emailDiv').css('display','block');
					$('#eventPlace').val(place);
					$('#startEventDate').val(sDate);
					$('#endEventDate').val(eDate);
					$('#startEventTime').val(sTime);
					$('#endEventTime').val(eTime);
					$('#titleText').text(getValues(Labels,'Event'));
					$.ajax({ 
			           url: Path+'/calenderAction.do',
			       	   type:"POST",
				       data:{act:'fetchEventUsers',taskId:task_id},
				       error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
								}, 
				       success:function(result){
				      	parent.checkSessionTimeOut(result);
				      		$('#participantEmail').html(result);
				      		$('#participantEmailSection').append("<div id=\"eventCommentHeader\" class=\"eventNameLabel\" style=\"width: 73px;float: left;text-align: right;\">"+getValues(Labels,'Comments')+"</div>");
				      		var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        				$.ajax({ 
					           url: Path+'/calenderAction.do',
					       	   type:"POST",
						       data:{act:'getEventComments',taskId:task_id},
						       error: function(jqXHR, textStatus, errorThrown) {
						                checkError(jqXHR,textStatus,errorThrown);
						                $("#loadingBar").hide();
										timerControl("");
										}, 
						       success:function(result){
						      	parent.checkSessionTimeOut(result);
						      		$('#participantEmailSection').append(result);
						      		$('#eventCommentContainer').css({'max-height':'54px','width':'510px','padding-left':'10px'});
				      				$('#eventCommentHeader').css('width','65px');
					        		if(!isiPad)
				      					scrollBar('eventCommentContainer');
				      			}
						    });  		
				       		$('#participantEmail > div').each(function() {
								var id = $(this).attr('id');
								var userId = id.split('_')[1];
								$('#participant_'+userId).css('display','none');
							});
				       		if(!isiPad) {
				       			scrollBar('userListContainer');
								scrollBar('participantEmail');
							}
					if(projName != '-' && projName!='') {
						$('select#project').val(projName);
						$('select#userProject').val(projName);
						document.getElementById("project").value=document.getElementById("userProject").value;
						getProjectUsers(document.getElementById("userProject"));
					}	
					if(projName == getValues(Labels,'Select') || projName == ''){
						$('select#project').val(getValues(Labels,'Select'));
						$('select#userProject').val(getValues(Labels,'All_Users'));
						getProjectUsers(document.getElementById("userProject"));
					}
				       }
					});	
				}
				
				if(divId == 'assignedTasks'){
					$("#dependencyTask").show();
				//	$("#toDate").css("margin-left","24px");
					$('#dependencyTask').attr("onclick","updateListDependencyTask()");
					$('.createDependencyTask').attr("onclick","updateDependencyTaskList()");
				}
	       		$('#eventName').val(title);
			/*	if(!eDate) {
        			eDate = sDate;
        		}*/
        		$('#titleImgs').attr('src',Path+'/images/calender/'+taskImages+'Black.png');
				$('#titleImgs').attr('title',taskImages);
				$('#headerTitles').text(taskImages);
				if(divId != 'assignedEvent') {
					$('.estmHour').show();
					$('#startDateDiv').val(sDate);
					$('#endDateDiv').val(eDate);
					$('#startTime').val(sTime);
					$('#endTime').val(eTime);
					$('#totalHour').val(tHour);
					$('#totalMinute').val(tMinute);
					$('#pariticipateDiv').parent('div').css('display','none');
					$.ajax({ 
			           url: Path+'/calenderAction.do',
			       	   type:"POST",
				       data:{act:'fetchTaskUsers',taskId:task_id, taskType:divId},
				       error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
								}, 
				       success:function(result){
				       parent.checkSessionTimeOut(result);
				       		$('#participantEmail').html(result);
				       		$('.ui-slider-handle.ui-state-default.ui-corner-all').css('background', 'url("images/handleDisable.png") repeat-x scroll 50% 50% rgba(0, 0, 0, 0)');
							$( ".ui-slider-handle" ).on( "mousedown", function(event) {
							   parent.alertFun(getValues(companyAlerts,"Alert_CantUpProgress"),'warning');});
							$( ".userCompPerSlider" ).on( "click", function(event) {
							   parent.alertFun(getValues(companyAlerts,"Alert_CantUpProgress"),'warning');});
               				if(projId == '0'){
								getAllUsers();//	$('#userListContainer').html('${allUsers}');
						    	$('.odd, .even').each(function() {
									var id = $(this).attr('id');
									var userId = id.split('_')[1];
									$('#participant_'+userId).css('display','none');
								});
								var isiPad = navigator.userAgent.match(/iPad/i) != null;
			       				if(!isiPad) {
									scrollBar('userListContainer');
									scrollBar('participantEmail');
								}
							}else{	
								$("select#userProject").find("option#"+projId).attr("selected", "selected");
								$.ajax({ 
							       url: Path+'/calenderAction.do',
							       type:"POST",
							       data:{act:'fetchParticipants',projId:projId,userId:UserId},
							       error: function(jqXHR, textStatus, errorThrown) {
							                checkError(jqXHR,textStatus,errorThrown);
							                $("#loadingBar").hide();
											timerControl("");
											}, 
							       success:function(result){
							       		parent.checkSessionTimeOut(result);
							       		$('#userListContainer').html(result);
										$('.odd, .even').each(function() {
											var id = $(this).attr('id');
											var userId = id.split('_')[1];
											$('#participant_'+userId).css('display','none');
										});
										if($('[id^=participant_]:visible').length < 1){
											$('#userListContainer').append('<div id="innerDiv19" align="center"> No Participant</div>');
										}
										var isiPad = navigator.userAgent.match(/iPad/i) != null;
				        				if(!isiPad) {
				        					scrollBar('userListContainer');
											scrollBar('participantEmail');
										}
							       }
								});
							}
			       			$(document).click(function() {     // all dropdowns
								$('.wrapper-dropdown-2').removeClass('active');
							});   }
			       	   });
				}
				if(divId != 'assignedTasks' && divId != 'assignedEvent' && divId!='assignedDocument'){
					$("#userProject,#project").prop("disabled", true);
					
				}	
				if(divId == 'assignedTasks' || divId == 'assignedIdea' || divId == 'assignedEvent'){
							   tflag=true;
							   $("#taskDocCreateImg").show();
							   $(".uploadTaskIcon").show();
							   if(divId == 'assignedTasks'){
							   		$('#createOrCancel').css('width','333px');
							   }
							   else{
							   		$('#createOrCancel').css('width','200px');
							   }
						}else{
						   $("#taskDocCreateImg").hide();
						}
				parent.$('#loadingBar').hide();
				parent.timerControl("");}
		});
	}
	else if(divId == 'myTasks' || divId == 'myDocument' || divId == 'myWorkspaceDocument' || divId == 'myWorkFlow' || divId == 'myWorkflow' || divId == 'myEvent' || divId == 'myEvents' || divId== 'myIdea' || divId == 'mySprint') {
	 $.ajax({ 
	           url: Path+'/calenderAction.do',
	       	   type:"POST",
		       data:{act:'getMyTaskDetails',taskId:myTaskId,parId:divId},
		       error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
		       success:function(result){
	            parent.checkSessionTimeOut(result);
	         	$("#eventPopup").hide();
	       		$("#taskPopupSection").hide();
       			$('.transparent').show();
       			parent.$("#transparentDiv").show();
       			$('#taskCalendar').append(result);
       			$('.ui-slider-handle.ui-state-default.ui-corner-all').css('background', 'url("images/handleDisable.png") repeat-x scroll 50% 50% rgba(0, 0, 0, 0)');
       			$( ".userCompPerSlider" ).on( "click", function(event) {
							   parent.alertFun(getValues(companyAlerts,"Alert_CantUpProgress"),'warning');});
				$( ".userCompPerSlider" ).find(".ui-slider-handle").on( "mousedown", function(event) {
							   parent.alertFun(getValues(companyAlerts,"Alert_CantUpProgress"),'warning');});
       			var tAmount = $('#taskAmount').val();
       			if(tAmount < 100)
       				$('#slider-range-min > a.ui-slider-handle').css('background', 'url("scripts/slider/css/ui-lightness/images/handle.png") repeat-x scroll 50% 50% rgba(0, 0, 0, 0)');
       			if(divId == 'myEvent' || divId == 'myEvents')	
       				$(".removeEventUser").hide();	
				if(height >= 550)
					$('.eventPopup').css('height','550px');
				else {
					$('.eventPopup').css({'height': height+'px','margin-top':'','margin-top':'-'+height/2+'px'});
				}
				if(divId == 'mySprint'){
					loadCustomLabel('TaskUI');
					$('.sprintUserContainer').css('width','715px');
					$('.sprintUserContainer').css('margin-left','20px');
					$('.userTitle').css('width','230px');
					$('.slider').css('width','235px');
					$('.completePer').css('width','50px');
					$('.userContainerSlide').css('width','150px');
					$('.taskAmount').css('margin-left','30px');
					$('.userStatus').css('width','155px');
					$('.userWorkingHour').css('text-align','left');
					$('.userComments').css('float','right');
					$('.usrComments').css('font-size','12px');
				}
				var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        	if(divId == 'myEvent' || divId == 'myEvents'){
	        		if(!isiPad){
       					scrollBar('postedComments');
       					scrollBar('eventUserContainer');
       					if(height < 550){
							$('.eventPopup').css('width','660px');
							scrollBar('myTaskPopup');
						}	
       				}$.ajax({ 
					           url: Path+'/calenderAction.do',
					       	   type:"POST",
						       data:{act:'getEventComments',taskId:myTaskId},
						       error: function(jqXHR, textStatus, errorThrown) {
						                checkError(jqXHR,textStatus,errorThrown);
						                $("#loadingBar").hide();
										timerControl("");
										}, 
						       success:function(result){
						      	parent.checkSessionTimeOut(result);
						      		$('#postedComments').append(result);
						      		$('#eventCommentContainer').css({'max-height':'100px','width':'570px','padding-left':'10px','margin-left':'30px'});
				      				$('#eventCommentHeader').css('width','65px');
				      				$('[id^=commentTextarea_]').prop("disabled",false);
				      				if(!isiPad)
				      					scrollBar('eventCommentContainer');
				      			}
						    });
					//$("#taskDocCreateImg").hide();
					$("#updateOrCancel").css('width','28%');
					$("#updateTask").css('width','39%');
					$("#cancelTask").css('width','37%');
       			}	
       			else {	
	       			taskHour = $('#workingHours').val();
	       			if(!isiPad){
						commentsScrollBar();
						if(height < 550){
							$('.eventPopup').css('width','820px');
							scrollBar('myTaskPopup');
						}	
					}	
				}
				$(".taskSub").each(function(){
                	var title = $(this).attr('title').replaceAll("CHR(41)"," ").replaceAll("CHR(39)","'");
                	$(this).attr('title',title);
				});
				
				if(divId == 'myTasks' || divId == 'mySprint' || divId == 'myIdea' || divId == 'myEvent' || divId == 'myEvents'){
							   tflag=true;
							   $("#taskDocCreateImg").show();
							   $(".uploadTaskIcon").show();
							   $('#createOrCancel').css('width','200px');
						}else{
						   $("#taskDocCreateImg").hide();
						}
				parent.$('#loadingBar').hide();	
				parent.timerControl("");
			}
		});
	}
	else if(divId == 'myTask' || divId == 'myDocumentTask' || divId == 'myWsDocumentTask' || divId == 'myWorkflowTask' || divId == 'myworkflowTask' || divId == 'myEventTask' || divId == 'myIdeaTask' || divId == 'mySprintTasks' || divId == 'mySprintTask' || divId == 'assignedTask' || divId == 'assignedDocumentTask' || divId == 'assignedWsDocumentTask' || divId == 'assignedWorkflowTask' || divId == 'assignedworkflowTask' || divId == 'assignedEventTask' || divId == 'assignedIdeaTask' || divId == 'assignedSprint' || divId == 'assignedSprintTask') {
	     $.ajax({ 
	           url: Path+'/calenderAction.do',
	       	   type:"POST",
		       data:{act:'getHistoryMyTaskDetails',taskId:myTaskId,parId:divId},
		       error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
		       success:function(result){
		       		parent.checkSessionTimeOut(result);
		            $("#historyeventPopup").hide();
	       			$('.transparent').show();
	       			parent.$("#transparentDiv").show();
	       			$("#taskCalendar").append(result);
	       			$('.ui-slider-handle.ui-state-default.ui-corner-all').css('background', 'url("images/handleDisable.png") repeat-x scroll 50% 50% rgba(0, 0, 0, 0)');
	       			$( ".ui-slider-handle" ).on( "mousedown", function(event) {
							   parent.alertFun(getValues(companyAlerts,"Alert_CantUpProgress"),'warning');});
					$( ".userCompPerSlider" ).on( "click", function(event) {
							   parent.alertFun(getValues(companyAlerts,"Alert_CantUpProgress"),'warning');});
	       			$("#historymyTaskPopup").show();
	       			isHistory = 'yes';
	       			parent.$('#loadingBar').hide();
	       			parent.timerControl("");
	       			var isiPad = navigator.userAgent.match(/iPad/i) != null;
	       			if(!isiPad){
	       				if(height < 400){
							$('#historymyTaskPopup').css({'width':'650px','height':height+'px'});
							$('.eventPopup').css({'margin-top':'','margin-top':'-'+height/2+'px'});
							scrollBar('historymyTaskPopup');
						}
						if(divId == 'assignedTasks' || divId == 'myTask' || divId == 'mySprint' || divId == 'myIdeaTask'){
							   tflag=true;
							   $("#taskDocCreateImg").show();
							   $(".uploadTaskIcon").show();
							   $('#createOrCancel').css('width','200px');
						}else{
						   $("#taskDocCreateImg").hide();
						}
						if(divId == 'mySprintTasks' || divId == 'mySprintTask'){
							loadCustomLabel('TaskUI');
							$('.sprintUserContainer').css('width','590px');
							$('.sprintUserContainer').css('margin-left','20px');
							$('.userTitle').css('width','165px');
							$('.slider').css('width','242px');
							$('.userCompPerSlider').css('width','170px');
							$('.userWorkingHour').css('width','50px');
							$('.userStatus').css('width','80px');
							$('.userComments').css('float','left');
							$('.usrComments').css('font-size','12px');
							$('.deleteUser').remove();
						}
	       				if(divId == 'myEventTask'){
		       				scrollBar('eventCommentContainer');
		        			scrollBar('eventUserContainer');
		        		}
		        		else
	       				commentsScrollBar();
	       			}
	       			$(".taskSub").each(function(){
	                	var title = $(this).attr('title').replaceAll("CHR(41)"," ").replaceAll("CHR(39)","'").replaceAll("CHR(40)","\"");
	                	$(this).attr('title',title);
					});
				}
	  });
	}
	
	
}
	
function closePopup(){   /* Task Popup close */
	$("#dependencyTask").hide();
	$("#dependencyTaskPopup").hide();
	$("#dependencyListTaskPopup").hide();
	$('#dependencyTaskList').html("");
	$("#selectedDependencyTaskDiv").find('#selectedDependencyTaskContent').html("");
	taskDependencyId="";
	tempNewDepId="";
	updateNewDepId="";
	remDependencyId="";
	updateRemoveDepId="";
	$("select#sortTasks").val('All');
	$("select#sortInWs").val('sort');
	$('select#dependencyTaskDropDown').val('Select');
	$('select#dependencyUpdateTaskDropDown').val('Select');
	$("#myTaskPopup").remove();
	$("#historymyTaskPopup").remove();
	$("#historyeventPopup").remove();
	$('#ideaTaskContainer').remove();
	$('#taskUserListContainer').remove();
	$('#eventCommentHeader').remove();
	$('#CreateNewCsvPopUp').hide();
	$('#eventCommentContainer').remove();
	$('#taskWFDetails').html("");
	$('#totalHour').val('0');
	$('#totalMinute').val('0');
	
	$("#eventName").css('width','412px');
	$("#docDownload").hide();
	$('#checkAll').removeClass().addClass('removeAll');
	hideTaskHourDiv();
	$('#projectUsers').css({'width':'278px','margin-left':'0'});
	$('#totalMinute').css('border','1px solid #BFBFBF');
	parId="";
	isHistory = '';
	recPatternValue='';
	recType='';
	recEnd='';
	taskDateXml="";
	confirmId ='';
	prevSDate='';
	prevEDate='';
	emptyTaskDetails();
	$('select#userProject').val(getValues(Labels,'All_Users'));
	$('#eventParticipants').css('border','');
	$('#totalHour').css('border','1px solid #BFBFBF');
	$('.addParticipant').hide();
	$('input#actualWorkHour').attr('value','0').css('color','#848484');
	$('input#actualWorkMinute').attr('value','0').css('color','#848484');
	$('.createEvent').css('display','block');
	$('.createEvent1').css('display','none');
	$('#recurrencePopupDiv').hide();
	$('#recurrenceTxt').empty();
	$('#recStart').val('');
	$('#recEnd').val('');
	$('#participantEmail').attr('class','taskPrtcpntSection');
	$('#selectTask').val(getValues(Labels,'Task'));	
	$('#userSearch').val('');	
	$('.Create').text(getValues(Labels,'Save'));
	selectedTaskemailId="";
	removeEmailId="";
	wFTaskDateXml = "";
	deletedUserID="";
	mid='';
	modelId='';
	projId='';
	$("#unassgntaskDiv").css('display','block');
	$("#assgntaskDiv").css('display','none');
	$('#projectSection').css('display','block');
	$('#prjSection').css('display','block');
	$('#taskSection').css('display','none');
	$('#tskSection').css('display','none');
	$('#pariticipateDiv').parent().css('display','block');
	$('#titleImg').attr('src',Path+'/images/calender/TaskBlack.png');
	$('#addParticipantTasks').removeClass();
	$('#addParticipantTasks').addClass('addTaskEmail');
	$('#emailDiv').hide();
	$('.calInstructions').text($("#taskMsg").val());
	$('#titleText').text(getValues(Labels,'Task'));
	if(headerText == 'workflow') {
		mid='';
		$('#eventPopup').removeClass('workFlowPopup').addClass('eventPopup');
		$('#wfShowTask').remove();
		$('#taskWFDetails').html('');
		$('#taskDetails').css('display','block');
		$('#addParticipantTasks').css('display','block');
		$('#workFlowContainer').css('display','none');
		$('.addParticipant').css('diaplay','block');
		$("#wfTaskTemplateView").css('display','none');
		$('#dd7 span').text(getValues(Labels,'Select'));
	}
	$('#estmatedHour').val('0');
	$('#estmatedMinute').val('0');
	$("#eventPopup").hide();
	$('.eventDateDiv').hide();
	$('.calInstructions').show();
    $('#addParticipantTasks').css('height','173px');
    $('#participantEmail').css('height','145px');
	$('.taskDate').show();
	$('#selectPlace').css('display','none');	
	$('#pariticipateDiv').css('padding-top','10px');
	$("#myTaskPopup").remove();
	$('#participantEmail').css('width','98%');
	$("#historymyTaskPopup").remove();
	$("#historyeventPopup").remove();
	$('#ideaTaskContainer').remove();
	$('#taskUserListContainer').remove();
	$("#userProject,#project").prop("disabled", false);
	resetDateValidate='0';
	resetDateVar='0';
	prevDateStartSave='';
	prevDateEndSave='';
	prevHourVal='0';
	prevMinuteVal='0';
	resetDateValidate='0';
	if(height < 550){
		$("#eventPopup").mCustomScrollbar("destroy");
		$('#myTaskPopup').mCustomScrollbar("destroy");
		$('#historymyTaskPopup').mCustomScrollbar("destroy");
	}
	$('.transparent').hide();
	parent.$("#transparentDiv").hide();
	$('#taskEhourPopupDiv').hide();
	$('#searchUserImg').attr('src',Path+'/images/calender/search.png');
	$("#moreButton").show();
	defaultCss();
	$("#createOrCancel").css('width','333px');
	
}
	
		
var contentData="completeData";	
function addTasks(){  				/*Create Task functionality */
	var hdrTxt = $('#selectTask').val();					
	if(hdrTxt == 'WorkFlow') {
		addWFTask();
		return null;
	}
	else {
		title = $('#eventName').val();
		$('#eventName').css('border','');
		if(hdrTxt == 'Task'){
			$('#startDateDiv').css('border','');
			$('#endDateDiv').css('border','');
			$('#endTime').css('border','');
			start = $('#startDateDiv').val();
			end = $('#endDateDiv').val();
			startTime = $('#startTime').val();
			endTime = $('#endTime').val();
		}
		else {
			$('#startEventDate').css('border','');
			$('#endEventDate').css('border','');
			$('#endEventTime').css('border','');
			$('#eventPlace').css('border','');
			start = $('#startEventDate').val();
			end = $('#endEventDate').val();
			startTime = $('#startEventTime').val();
			endTime = $('#endEventTime').val();
		}
	    if(start == end) {
			var st = parseInt(startTime.replace(':', ''), 10); 
        	var et = parseInt(endTime.replace(':', ''), 10);
			if(st >= et)
            {
               parent.alertFun(getValues(customalertData,"Alert_ValidEndTime"),'warning');
               $('#endTime').css("border","1px solid red");
               return null;
            }
		}
		if(!startTime) {
			startTime = '00:00';
		}
		if(!endTime) {
			endTime = '23:59';
		}
		var event_id='';
		var taskName = $('#dd span').text();
		var taskType = $('#selectTask').val();
		if(taskName == getValues(Labels,'ASSIGNED_TASKS')) {
			if(hdrTxt == 'Event') {
				taskName='assignedEvent';
			}
			else
				taskName='assignedTasks';
			var backgroundColor='#F5CB02';
			var borderColor='#FBAD3D';
		}
		if(taskName == getValues(Labels,'MY_TASKS')) {
			if(hdrTxt == 'Event') {
				taskName='myEvent';
			}
			else
				taskName='myTasks';
			var backgroundColor='#2EB5E0';
			var borderColor='#49BFE6';
		}
		if(!title || title.match(/^\s*$/)) {
			 $('#eventName').css("border","1px solid red");
			 return null;
		}
			var sdArray=start.split('-');
			var sd=sdArray[2]+'/'+sdArray[0]+'/'+sdArray[1];
			var edArray=end.split('-');
			var ed=edArray[2]+'/'+edArray[0]+'/'+edArray[1];
			var d1=new Date(sd);
			var d2=new Date(ed);
			if(d1 > d2){
			  parent.alertFun(getValues(customalertData,"Alert_InvalidDate"),'warning');
			 if(hdrTxt == 'Event') {
			 $('#startEventDate').css("border","1px solid red");
			 $('#endEventDate').css("border","1px solid red");
			 }else{
			 $('#startDateDiv').css("border","1px solid red");
			 $('#endDateDiv').css("border","1px solid red");
			 }
			return false;
			}
		if(hdrTxt == 'Event') {
//commented for Incomplete Task functionality  OLD Functionlity******************		
	/*		if(!start) {					
				$('#startEventDate').css("border","1px solid red");
				return null;
			}	
			if(!end) {
				$('#endEventDate').css("border","1px solid red");
				return null;
			}
			
			if(!place || place.match(/^\s*$/)) {
				$('#eventPlace').css('border','1px solid red');
				return null;
			}		*/
//*****************************************************************			
			place = $('#eventPlace').val();		
			
//commented for Incomplete Task functionality  OLD Functionlity******************	
//commented incomplete functionality
			if((!start) || (!end) || (!place) || ($('.participantDetails').length <1 && $('.otherUserEmail').length <1 ) || ($('#participantEmail').children('div').length<=0) ){
				if(contentData!="incompleteData"){
			    // 	contentData="incompleteData";
					parent.confirmFun(getValues(companyAlerts,"Alert_SaveIncompleteData"),"delete","goToAddTask");
					return false;	
				}
				
			}
		}
		if(hdrTxt == 'Task') {
//****************OLD Functionlity****************************************
/*
			if(!start) {
				$('#startDateDiv').css("border","1px solid red");
				return null;
			}	
			if(!end) {
				$('#endDateDiv').css("border","1px solid red");
				return null;
			}
*/			
//***************************************************************************
			var sdArray=start.split('-');
			var sd=sdArray[2]+'/'+sdArray[0]+'/'+sdArray[1];
			var edArray=end.split('-');
			var ed=edArray[2]+'/'+edArray[0]+'/'+edArray[1];
			var d1=new Date(sd);
			var d2=new Date(ed);
			if(d1 > d2){
			  parent.alertFun(getValues(customalertData,"Alert_InvalidDate"),'warning');
			  $('#startDateDiv').css("border","1px solid red");
			  $('#endDateDiv').css("border","1px solid red");
			  return false;
			}
			if($.isNumeric($('#totalHour').val()) == false || $('#totalHour').val() < 0){
	         	   parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
	 		       $('#totalHour').css('border','1px solid red');
	 		       return null;
		        }
		   if($.isNumeric($('#totalMinute').val()) == false){
	         	   parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
	 		       $('#totalMinute').css('border','1px solid red');
	 		       return null;
		        } 
		        
		   if($('#totalMinute').val()>59 || $('#totalMinute').val()<0){
		   		   parent.alertFun(getValues(companyAlerts,"Alert_MinValue"),'warning');
	 		       $('#totalMinute').css('border','1px solid red');
	 		       return null;
		   
		   }    
	    }else
     		$('#totalHour').css('border','1px solid #BFBFBF');
//******************************NEW functionality***************************************************************************************   		
     		if((!start) || (!end) || ($('.participantDetails').length <1 && $('.otherUserEmail').length <1 ) || ($('#participantEmail').children('div').length<=0) ){
				if(contentData!="incompleteData"){
			    // 	contentData="incompleteData";
					parent.confirmFun(getValues(companyAlerts,"Alert_SaveIncompleteData"),"delete","goToAddTask");
					return false;	
				}
				
			}				
//************************************************************************************************************************			
     		$('#totalMinute').css('border','1px solid #BFBFBF');
			var isiPad = navigator.userAgent.match(/iPad/i) != null;
//****************OLD Functionlity****************************************
      /*    	if(!isiPad) {
          				if($('.participantDetails').length <1 && $('.otherUserEmail').length <1 ){		          			
			          			parent.alertFun(getValues(customalertData,"Alert_Participant"),'warning');
			          			return false;
	          			}
          			}else{
          				if($('#participantEmail').children('div').length<=0){
		          			parent.alertFun(getValues(customalertData,"Alert_Participant"),'warning');
		          			return false;
          			    }
          			}	*/
  //**********************************************************************************************        			
		var textColor = '#FFFFFF';
		repeat = $('.eventReminder').val();
		description = $('#eventDescription').val();
		if(!description) {
			var description = '';
		}
		var updateStart = start;
		var updateEnd = end;
		var reminder = $('#eventReminder').val();
		var remType = $('#remindThrough').val();
		tHour = $('#totalHour').val();
		tMinute=$('#totalMinute').val();
		if(reminder == getValues(Labels,'Select')) {
			reminder = null;
		}	
		if(remType == getValues(Labels,'Select')) {
			remType = null;
		}			
		if(taskDateXml == '' || taskDateXml == 'null' || taskDateXml == 'undefined' || estmHour != tHour || estmMinute != tMinute) {
			 tmpSDate = start;
		 	 tmpEDate = end;
		 	 var strD=tmpSDate.split("-");
			 var str_date=strD[2]+"/"+strD[0]+"/"+strD[1];
			 var strE=tmpEDate.split("-");
			 var end_date=strE[2]+"/"+strE[0]+"/"+strE[1];
			var tskDates = new Array();
			var tskMonth = new Array();
			var tskYear = new Array();
			var tskDays = new Array();
			var weekday=new Array(7);
			weekday[0]=day0;
			weekday[1]=day1;
			weekday[2]=day2;
			weekday[3]=day3;
			weekday[4]=day4;
			weekday[5]=day5;
			weekday[6]=day6;
			estmHour = $('#estmatedHour').val();
			tHour = $('#totalHour').val();
			
			estmMinute = $('#estmatedMinute').val();
			tMinute = $('#totalMinute').val();
			
			var startDte = new Date(str_date);
		    var futureDte = new Date(end_date);
		    var range = []
		    var  mil = 86400000 //24h in milliseconds
		    var minuteValue=$('#totalMinute').val();
		    var curHour=0;
		    var curMinute=0;
			var totalDay = (futureDte-startDte)/mil +1;
		    var hr_day=tHour/totalDay;
		    var hr_per_day=Math.round(hr_day);
		    var flag=0;
		    
		    
		    var min_day=tHour/totalDay;
		    var min_per_day=Math.round(min_day);
		    var flag_min=0;
		    var j=futureDte.getTime();
		    if((tHour>0) && (hr_per_day==0) ){
			   hr_per_day=tHour;
			}
			if((tMinute>0) && (min_per_day==0) ){
			   min_per_day=tHour;
			} 
		     taskDateXml = "<taskHour>";
		     for (var i=startDte.getTime(); i<=futureDte.getTime();i=i+mil) {
		     	curHour = parseInt(curHour,10) + parseInt(hr_per_day,10);
		     	curMinute = parseInt(curMinute,10) + parseInt(min_per_day,10);
	     		if(curHour < tHour) {
	     			hr_per_day = hr_per_day;
	     			if(i==j){
				  	 hr_per_day= parseInt(tHour,10) - (parseInt(curHour,10) - parseInt(hr_per_day,10));
					} 
	     		}
	     		else {
	     			if(flag == 0){
	      				curHour = curHour - hr_per_day;
			       		hr_per_day = tHour - curHour;
			       		flag = 1;
		      		}
		      		else
		      			hr_per_day = 0;
				}
				
				if(curMinute < tMinute) {
	     			min_per_day = min_per_day;
	     			if(i==j){
				  	 min_per_day= parseInt(tMinute,10) - (parseInt(curMinute,10) - parseInt(min_per_day,10));
					} 
	     		}
	     		else {
	     			if(flag_min == 0){
	      				curMinute = curMinute - min_per_day;
			       		min_per_day = tMinute - curMinute;
			       		flag_min = 1;
		      		}
		      		else
		      			min_per_day = 0;
				}
				
				tskDates[i] = new Date(i).getDate();
			    tskMonth[i] = new Date(i).getMonth()+1;
			    tskYear[i] = new Date(i).getFullYear();
			    tskDays[i] = new Date(i).getDay();
			    var day = weekday[tskDays[i]]; 
			    var month = tskMonth[i];
			    if(month < 10) {
			    	month = '0'+month;
			    }
			    if(tskDates[i] < 10) {
			    	tskDates[i] = '0'+tskDates[i];
			    }
		      taskDateXml +="<taskDate id=\""+i+"\">"
			  taskDateXml +="<date>"+tskDates[i]+"/"+month+"/"+tskYear[i]+"</date>";
			  taskDateXml +="<day>"+day+"</day>";
			  taskDateXml +="<hour>"+hr_per_day+"</hour>";
			  taskDateXml +="<min>"+min_per_day+"</min>";
	 		  taskDateXml +="<status>Checked</status>";
	 		  taskDateXml +="</taskDate>";
	 		  if((tHour>0) && (hr_per_day==0) ){
	 		  	flag = 1;
	 		  }
	 		  if((tMinute>0) && (min_per_day==0) ){
	 		  	flag_min = 1;
	 		  }
			}	
			taskDateXml +="</taskHour>";
	    }
	    if(hdrTxt == 'Task') {   /* Add Task */		
	    	tHour = $('#totalHour').val();							
			parent.$('#loadingBar').show();
			$('#totalMinute').css('border','1px solid #BFBFBF');
			parent.timerControl("start");
			$.ajax({ 
		       url: Path+'/calenderAction.do',
		       type:"POST",
		       data:{act:'addtask',taskType:taskType,title:title,start:start,end:end,startTime:startTime,endTime:endTime,repeat:repeat,description:description,eId:eId,taskProject:projId,priorityId:priorityId,update:update,selectedTaskemailId: selectedTaskemailId,reminder:reminder,reminderType:remType,taskDateXml : taskDateXml,recPatternValue:recPatternValue,recEnd:recEnd ,recType:recType,estimatedHour:tHour,estimatedMinute:minuteValue,taskDependencyId:taskDependencyId},
		       error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
		       success:function(result){
		       	parent.checkSessionTimeOut(result);
		       	if(view=='calendarV'){
			        event_id = result.split('#@#@')[1];
			        	start = start.split('-');
			        	start = start[2]+'-'+start[0]+'-'+start[1];
			        	end = end.split('-');
			        	end = end[2]+'-'+end[0]+'-'+end[1];
			  			start = start+" "+startTime;
						end = end+" "+endTime;
						var assignedTaskDatas = result.split('#@#@')[0];
			        	var myTaskDatas = result.split('#@#@')[2];
			        	var historyDatas = result.split('#@#@')[3];
			        	calendarViewDatas(myTaskDatas,assignedTaskDatas,historyDatas);
			        	parent.$('#loadingBar').hide();
			        	parent.timerControl("");			
					if(update == 'no') {			
						taskCalendar($('#dd span').text());
			        }
					if(update == 'yes') {	
						calendar.fullCalendar('updateEvent', curr_event);
						calendar.fullCalendar('refetchEvents');
					}	
				}				
				else if(view=='listV' && moreValue=='myTasks'){
					showMyTaskList();
				}
				else if(view=='listV' && moreValue=='assignedTasks'){
					showAssignedTaskList();
				}
				taskDependencyId="";
				contentData="completeData";
			  }
			});
			taskDependencyId="";
		}
		if(hdrTxt == 'Event') {   /* Add Event */
			parent.$('#loadingBar').show();
			parent.timerControl("start");
			$.ajax({ 
		       url: Path+'/calenderAction.do',
		       type:"POST",
		       data:{act:'addEvent',title:title,start:start,end:end,startTime:startTime,endTime:endTime,place:place,description:description,eId:eId,taskProject:projId,priorityId:priorityId,update:update,selectedTaskemailId:selectedTaskemailId,selectedOtherEmail:selectedOtherEmail,reminder:reminder,reminderType:remType},
		       error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
		       success:function(result){
		       	parent.checkSessionTimeOut(result);
		       	if(view=='calendarV'){
			        event_id = result.split('#@#@')[1];
			        	start = start.split('-');
			        	start = start[2]+'-'+start[0]+'-'+start[1];
			        	end = end.split('-');
			        	end = end[2]+'-'+end[0]+'-'+end[1];
			  			start = start+" "+startTime;
						end = end+" "+endTime;
						var assignedTaskDatas = result.split('#@#@')[0];
			        	var myTaskDatas = result.split('#@#@')[2];
			        	var historyDatas = result.split('#@#@')[3];
			        	$('#myTasksList').html('');
			        	$('#assignedTasksList').html('');
			        	$('#historyListTasks').html('');
			        	calendarViewDatas(myTaskDatas,assignedTaskDatas,historyDatas);
			        	parent.$('#loadingBar').hide();
			        	parent.timerControl("");
			        	
			        	if(update == 'no') {
							taskCalendar($('#dd span').text());
				        }
			        /*if(update == 'no') {
						calendar.fullCalendar('renderEvent', {id:event_id, title: title, start: start, end: end, backgroundColor:backgroundColor,borderColor:borderColor, textColor:textColor,allDay : false ,timeFormat: 'H(:mm)',editable: true,taskType:taskName,ignoreTimezone: true,stick:true},true);
						calendar.fullCalendar('refetchEvents');
					}*/
					if(update == 'yes') {
						calendar.fullCalendar('updateEvent', curr_event);
						calendar.fullCalendar('refetchEvents');
					}
				}
				else if(view=='listV' && moreValue=='myTasks'){
					showMyTaskList();
				}
				else if(view=='listV' && moreValue=='assignedTasks'){
					showAssignedTaskList();
				}
				contentData="completeData";
			  }
			});
		}
	}closePopup();	
}	
function goToAddTask(){
	contentData="incompleteData";
	addTasks();
}
	