	        var company = "";
			var companySize = "";
			var fname="";
			var fnlen = "";
			var lname = "";
			var email = "";
			var username = "";
			var address1 = "";
			var city = "";
			var country ="";
			var phone = "";
			var timezone = "";
	        var formName="";
	function submitRegistration(){
	    var aVal = $('#register_type').val();
	    
	    if(aVal == 'accType_enterprise'){
	        formName="ente-form";
		    company = $("#ent_company").val();
			companySize = $("#ent_companySize").val();
			fname=$("#ent_fname").val();
			fnlen = $.trim(fname).length;
			lname = $("#ent_lname").val();
			email = $("#ent_email").val();
			username = $("#ent_username").val();
			address1 = $("#ent_address1").val();
			city = $("#ent_city").val();
			country = $("#ent_country").val();
			phone = $("#ent_phone").val();
			timezone = $('select#ent_timezone option:selected').val();
			
			if(company == null || company == ""){
				//alertFun("<bean:message key='Company_Name_cannot_be_empty'/>",'success');
				alert("Company name cannot be empty!");
				$("#ent_company").focus();
				return false;
			  }
		    
			
			if(fname == null || fname == ""){
				//alertFun("<bean:message key='First_Name_cannot_be_empty'/>",'success');
				alert("First Name cannot be empty!");
				$("#ent_fname").focus();
				return false;
			}else{
				if(fnlen==0) {
					//alertFun("<bean:message key='First_Name_should_not_start_with_space'/>",'success');
					alert("First Name should not start with space!");
					$("#ent_fname").focus();
					return false;
				} 
		  	}
			
			if(email=="" || email==null){
				//alertFun("<bean:message key='Email_cannot_be_empty'/>",'success');
				alert("Email cannot be empty!");
			 	$("#ent_email").focus();
				return false;
			}else{
			 	if(!validateEmailJs(email,true,true) ) {
				 	$("#ent_email").focus();
					return false;
			 	} 
			}
			
			if(username=="" || username==null){
				//alertFun("<bean:message key='Username_cannot_be_empty'/>",'success');
				alert("Username cannot be empty!");
			 	$("#ent_username").focus();
				return false;
			}
			
			if(country==""||country==null){
				//alertFun("<bean:message key='Country_cannot_be_empty'/>",'success');
				alert("Country cannot be empty!");
			 	$("#ent_country").focus();
				return false;
			}
				
		}else{
		    formName="stan-form";
		    company = $("#stan_company").val();
			companySize = $("#stan_companySize").val();
			fname=$("#stan_fname").val();
			fnlen = $.trim(fname).length;
			lname = $("#stan_lname").val();
			email = $("#stan_email").val();
			username = $("#stan_username").val();
			address1 = $("#stan_address1").val();
			city = $("#stan_city").val();
			country = $("#stan_country").val();
			phone = $("#stan_phone").val();
			timezone = $('select#stan_timezone option:selected').val();
			
			
			if(fname == null || fname == ""){
				//alertFun("<bean:message key='First_Name_cannot_be_empty'/>",'success');
				alert("First Name cannot be empty!");
				$("#stan_fname").focus();
				return false;
			}else{
				if(fnlen==0) {
					//alertFun("<bean:message key='First_Name_should_not_start_with_space'/>",'success');
					alert("First Name should not start with space!");
					$("#stan_fname").focus();
					return false;
				} 
		  	}
			
			if(email=="" || email==null){
				//alertFun("<bean:message key='Email_cannot_be_empty'/>",'success');
				alert("Email cannot be empty!");
			 	$("#stan_email").focus();
				return false;
			}else{
			 	if(!validateEmailJs(email,true,true) ) {
				 	$("#stan_email").focus();
					return false;
			 	} 
			}
			
			if(username=="" || username==null){
				//alertFun("<bean:message key='Username_cannot_be_empty'/>",'success');
				alert("Username cannot be empty!");
			 	$("#stan_username").focus();
				return false;
			}
			
			if(country==""||country==null){
				//alertFun("<bean:message key='Country_cannot_be_empty'/>",'success');
				alert("Country cannot be empty!");
			 	$("#stan_country").focus();
				return false;
			}
			
		}
		
		
		
		 
	    displayLoader();
		$.ajax({
	       	url: "/collaborationApp/register.do?act=register&accType="+aVal,
			type:"POST",
			data:$("#"+formName).serialize(),
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(data){
				checkSessionTimeOut(data);
				hideLoader();
				if(data == "success"){
					//alertFun("<bean:message key='Confirmation_is_sent_to_your_email'/>",'success');
					alert("Confirmation is sent to your email!");
	    		}else if(data == 'userexists'){
	    		   //alertFun("<bean:message key='User_already_exists_with_the_same_Username'/>",'success');
	    		   alert("User already exists with the same Username!");
	    		}else if(data == 'companyExists'){
	    			//alertFun("<bean:message key='Company_already_exists'/>",'success');
	    			alert("Company already exists");
	    			if(company == null || company == ""){
	    				//alertFun("<bean:message key='Company_Name_cannot_be_empty'/>",'success');
	    				alert("Company name cannot be empty!");
						$("#company").focus();
						return false;
					}
	    		}
			}
		});
		
		
	}

	
	function displayLoader(){
		//$('div#brfSubmit').removeAttr('onclick');
		//$('div#brfSubmit').css('cursor', 'default');
		$('img.aLoader').show();
	}
	
	function hideLoader(){
		//$('div#brfSubmit').attr('onclick', 'submitRegistration()');
		//$('div#brfSubmit').css('cursor', 'pointer');
		$('img.aLoader').hide();
	}
	
		
	function alertFun(info,type){
	 
     if(!info || !type){
            throw new Error("Please enter all the required config options!");
     }
     if(type=='warning'){
        name="BoxAlert";
     }else if(type=='error'){
        name="BoxError";
     }else{
        name="BoxSuccess";
     }
     
     var html='<div id="BoxOverlay" ></div>'+
    		 '<div id="alertDiv" class="alertMainCss">'+
    		 '  <div id="confirmCloseDiv" title="close" ></div> '+
             '  <div style="box-shadow: 0 0 20px #000000;" >'+
             '   <div style="box-shadow: 0 0 0 0 #000000; padding: 20px 20px 0;">'+
             '    <div id="confirm-BoxContenedor" class="'+name+'">'+
             '     <span id="confirmContent">'+info+'</span>'+   
             '     <div id="confirm-Buttons" class="alertBtnCss">'+
             '       <input id="BoxAlertBtnOk" type="submit" value="OK" >'+
             '     </div>'+
             '    </div>'+
             '  </div>'+
             ' </div>'+
             '</div>';
             
          $('body').find('#BoxOverlay').remove();
          $('body').find('#alertDiv').remove();
          //setTimeout(function(){  
                $('body').append(html);
                $('body').find('#alertDiv').find('#confirm-Buttons').children('#BoxAlertBtnOk').click(function(){
		          alertOk();
		        });
		        
		        $('body').find('#alertDiv').children('#confirmCloseDiv').click(function(){
		          alertOk();
		        });
       //},50);            
             
 
 }
 
  function alertOk(){
	   $('#BoxOverlay').hide();
	   $('#alertDiv').hide();
  }
  
  function autofillUsername(emailObj){
  	  var reg_type=$("#register_type").val();
  	  var emailVal = $(emailObj).val();
	  if(reg_type=='accType_enterprise'){
	    $('#ent_username').val(emailVal);
	  }else{
	    $('#stan_username').val(emailVal);
	  }
	}
  
  function clearFields(){
        $(".fname").val("");
		$(".email").val("");
		$(".lname").val("");
		$(".username").val("");
		$(".company").val("");
		$(".address1").val("");
		$(".zipPostalcode").val("");
		$(".phone").val("");
  
  }
  
  
  
  
  function validateEmailJs(addr,man,db) {
if (addr == '' && man) {
   if (db) alert('email address is mandatory');
   return false;
}
if (addr == '') return true;
var invalidChars = '\/\'\\ ";:?!()[]\{\}^|';
for (i=0; i<invalidChars.length; i++) {
   if (addr.indexOf(invalidChars.charAt(i),0) > -1) {
      if (db) alert('email address contains invalid characters');
      return false;
   }
}
for (i=0; i<addr.length; i++) {
   if (addr.charCodeAt(i)>127) {
      if (db) alert("email address contains non ascii characters.");
      return false;
   }
}

var atPos = addr.indexOf('@',0);
if (atPos == -1) {
   if (db) alert('email address must contain an @');
   return false;
}
if (atPos == 0) {
   if (db) alert('email address must not start with @');
   return false;
}
if (addr.indexOf('@', atPos + 1) > - 1) {
   if (db) alert('email address must contain only one @');
   return false;
}
if (addr.indexOf('.', atPos) == -1) {
   if (db) alert('email address must contain a period in the domain name');
   return false;
}
if (addr.indexOf('@.',0) != -1) {
   if (db) alert('period must not immediately follow @ in email address');
   return false;
}
if (addr.indexOf('.@',0) != -1){
   if (db) alert('period must not immediately precede @ in email address');
   return false;
}
if (addr.indexOf('..',0) != -1) {
   if (db) alert('two periods must not be adjacent in email address');
   return false;
}
var suffix = addr.substring(addr.lastIndexOf('.')+1);
	
if (suffix.length < 2 || suffix != 'com' && suffix != 'net' && suffix != 'org' && suffix != 'edu' && suffix != 'int' && suffix != 'mil' && suffix != 'gov' && suffix != 'biz' && suffix != 'aero' && suffix != 'name' && suffix != 'coop' && suffix != 'info' && suffix != 'pro' && suffix != 'museum' && suffix != 'in' && suffix != 'jobs' && suffix != 'mobi' && suffix != 'eu' && suffix != 'travel' && suffix != 'ac' && suffix != 'ad' && suffix != 'ae' && suffix != 'af' && suffix != 'ag' && suffix != 'ai' &&   suffix != 'al' && suffix != 'am' && suffix != 'an' && suffix != 'ao' && suffix != 'aq' && suffix != 'ar' && suffix != 'as' && suffix != 'at' && suffix != 'au' && suffix != 'aw' && suffix != 'az' && suffix != 'ax' && suffix != 'bl' && suffix != 'ba' && suffix != 'bb' && suffix != 'bd' && suffix != 'be' && suffix != 'bf' && suffix != 'bg' && suffix != 'bh' && suffix != 'bi' && suffix != 'bj' && suffix != 'bm' && suffix != 'bn' &&
            suffix != 'bo' && suffix != 'br' && suffix != 'bs' && suffix != 'bt' && suffix != 'bv' && suffix != 'bw' && suffix != 'by' && suffix != 'bz' && suffix != 'ca' && suffix != 'cc' && suffix != 'cd' && suffix != 'cf' && suffix != 'cg' && suffix != 'ch' && suffix != 'ci' && suffix != 'ck' && suffix != 'cl' && suffix != 'cm' && suffix != 'cn' && suffix != 'co' && suffix != 'cr' && suffix != 'cu' && suffix != 'cv' && suffix != 'cx' && suffix != 'cy' && suffix != 'cz' && suffix != 'de' && suffix != 'dj' && suffix != 'dk' && suffix != 'dm' && suffix != 'do' && suffix != 'dz' && suffix != 'ec' && suffix != 'ee' && suffix != 'eg' && suffix != 'eh' && suffix != 'er' && suffix != 'es' && suffix != 'et' && suffix != 'fi' && suffix != 'fj' && suffix != 'fk' && suffix != 'fm' && suffix != 'fo' && suffix != 'fr' && suffix != 'ga' && suffix != 'gd' && suffix != 'ge' && suffix != 'gf' && suffix != 'gg' && suffix != 'gh' && suffix != 'gi' && suffix != 'gl' && suffix != 'gm' && suffix != 'gn' && suffix != 'gp' && suffix != 'gq' && suffix != 'gr' && suffix != 'gs' && suffix != 'gt' && suffix != 'gu' && suffix != 'gv' && suffix != 'gy' && suffix != 'hk' && suffix != 'hm' && suffix != 'hn' && suffix != 'im' && suffix != 'in' && suffix != 'io' && suffix != 'iq' && suffix != 'ir' && suffix != 'is' && suffix != 'it' && suffix != 'je' && suffix != 'jm' && suffix != 'jo' && suffix != 'jp' && suffix != 'ke' && suffix != 'kg' && suffix != 'kh' && suffix != 'ki' && suffix != 'km' && suffix != 'kn' && suffix != 'kp' &&
            suffix != 'kr' && suffix != 'kw' && suffix != 'ky' && suffix != 'kz' && suffix != 'la' && suffix != 'lb' && suffix != 'lc' && suffix != 'li' && suffix != 'lk' && suffix != 'lr' && suffix != 'ls' && suffix != 'lt' && suffix != 'lu' && suffix != 'lv' && suffix != 'ly' && suffix != 'ma' && suffix != 'mc' && suffix != 'md' && suffix != 'mg' && suffix != 'mh' && suffix != 'mk' && suffix != 'ml' && suffix != 'mm' && suffix != 'mn' && suffix != 'mo' && suffix != 'mp' && suffix != 'mq' && suffix != 'mr' && suffix != 'ms' && suffix != 'mt' && suffix != 'mu' && suffix != 'mv' && suffix != 'mw' && suffix != 'mx' && suffix != 'my' && suffix != 'mz' && suffix != 'na' && suffix != 'nc' && suffix != 'ne' && suffix != 'nf' && suffix != 'ng' && suffix != 'ni' && suffix != 'nl' && suffix != 'no' && suffix != 'np' && suffix != 'nr' && suffix != 'nu' && suffix != 'nz' && suffix != 'om' && suffix != 'pa' && suffix != 'pe' && suffix != 'pf' && suffix != 'pg' && suffix != 'ph' && suffix != 'pk' && suffix != 'pl' && suffix != 'pm' && suffix != 'pn' && suffix != 'pr' && suffix != 'ps' && suffix != 'pt' && suffix != 'pw' && suffix != 'py' && suffix != 'qa' && suffix != 're' && suffix != 'ro' && suffix != 'rw' && suffix != 'ru' && suffix != 'sa' && suffix != 'sb' && suffix != 'sc' && suffix != 'sd' && suffix != 'se' && suffix != 'sg' && suffix != 'sh' && suffix != 'si' && suffix != 'sj' && suffix != 'sk' && suffix != 'sl' && suffix != 'sm' && suffix != 'sn' && suffix != 'so' && suffix != 'sr' && suffix != 'st' &&
            suffix != 'sv' && suffix != 'sy' && suffix != 'sz' && suffix != 'tc' && suffix != 'td' && suffix != 'tf' && suffix != 'tm' && suffix != 'tn' && suffix != 'to' && suffix != 'tt' && suffix != 'tz' && suffix != 'th' && suffix != 'tj' && suffix != 'tk' && suffix != 'tp' && suffix != 'tr' && suffix != 'ua' && suffix != 'ug' && suffix != 'uk' && suffix != 'um' && suffix != 'us' && suffix != 'uy' && suffix != 'uz' && suffix != 'va' && suffix != 'vc' && suffix != 've' && suffix != 'vi' && suffix != 'vg' && suffix != 'vn' && suffix != 'vu' && suffix != 'ws' && suffix != 'wf' && suffix != 'ye' && suffix != 'yt' && suffix != 'yu' && suffix != 'za' && suffix != 'zm' && suffix != 'zw'
           ) {   if (db) alert('invalid primary domain in email address');
   return false;
}
return true;
}
  