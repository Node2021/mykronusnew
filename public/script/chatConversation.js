/**
 * @author Abhishek 
 * This js file is used for showing the conversation happened with the user with latest message showing
 */

var jsonDataConversation ='';

$(function(){
	//resizeChatWindow();
		 $('img.conversation').click(function(){
		 $('#chat-frame,#chat-frame2').hide();
		 $('#chat-frame3').fadeIn();    	 
		 showPersonalConnectionMessages();
		 ScrollChat('conversationList');
		//window.opener.openChatWindowsecond();
		 
		 
	});
	
    $('#singleChat_conv').on('click',function(){
		 $('#chat-frame').fadeIn();
		 $('#chat-frame2,#chat-frame3').hide(); 
		 //namesFromColabusOffContacts();
		 ScrollChat('chat-box');
		//window.opener.openChatWindowsecond();
		parId="chatcontact";
	 });
    
	 
    
 	 $('#searchText3').on('keyup',function(){
 	
		    var txt = $('#searchText3').val().toLowerCase();
	
			 if(txt.length>0){
				  $('#searchCancel3').show();
			 }else{
				  $('#searchCancel3').hide();
			 } 
				  
				  
			 $('#conversationList li').each(function(){
			 
			     var val = $(this).find('.user_box_name').text().toLowerCase();
			      
					 if(val.indexOf(txt)!= -1)
			         {
					   $(this).show();
					 }else{
					   $(this).hide();
					 }
			 });			
		
		 });
    
	 $('#searchCancel3').on('click',function(){
		 $('#searchText3').val('');
		 $('#searchCancel3').hide();
		 $('#conversationList li').show();
	 });
	 
	 $('.groupChat').click(function(){
		
		 //GetgroupChatRooms();
		  
		 $('#chat-frame,#chat-frame3').hide();
		 $('#chat-frame2').fadeIn(); 
		 ScrollChat('groupChat-box');
		 //window.opener.openChatWindowsecond();
	 });
	 
	
	 	 
	    window.onbeforeunload = function () {
	    	window.opener.releaseChatSession();
	    	disconnect();
		  //  return "Do you really want to close+Conversation?";
		};
});


function showPersonalConnectionMessages(){
	vertiScrollBar = false;
	var localTZ=getTimeOffset(new Date());
    $.ajax({
		url: path+"/connectAction.do",
	    data:{act:"fetchPersonalMessagesChat",searchKey:"",searchStr:"",localTZ:localTZ},
	    mimeType: "textPlain",
	    success:function(result){
	    
			var data=result.split("##@@##");
			//console.log("data--"+data[0]);
		    	jsonDataConversation=jQuery.parseJSON(data[0]);
				loggedUname=data[1];
			if(data[0] == '[]'){
			   $("div#conversationList").html('<div align="center" style="overflow: auto; float: left; width: 100%;color: rgb(255, 255, 255); font-size: 14px; font-family: tahoma;">No Conversation</div>');
			   timerControl("");
		    }else{
			   result= prepareUIConversation();
			   $("div#conversationList ul").html(result);
			}
			
		}
		
	});
	return "sucess";	
}

function prepareUIConversation(){
	
	   var UI="";
	   var cid='';
	   var msgType = '';
	   var gname = '';
	   var sender_image_type = '';
	   var msg = '';
	   var sendTime = '';
	   var time = ''; 
	   var sender = '';
	   var imgName = '';
	   var groupMessCount = '';
	   var messageCount = '';
	   
    for(i=0; i<jsonDataConversation.length; i++){
    	
		    cid=jsonDataConversation[i].cid;	
		    msgType=jsonDataConversation[i].msgType;
		
		    gname =jsonDataConversation[i].gname;
	        sender_image_type=jsonDataConversation[i].sender_image_type;
	        msg=jsonDataConversation[i].msg;
	      //  msg = msg.replace("CH(50)","").replace("CH(50)","\n");
	        msg = replaceSpecialCharacter(msg);
	        sendTime=jsonDataConversation[i].sendTime;
	        time=sendTime.split('#@#');
	        sendTime=time[1]+", "+time[0];
	    
	        sender=jsonDataConversation[i].sender;
	        if(sender_image_type == null || sender_image_type=="" || sender_image_type == "null"){
	        	imgName='';
	        	
	        }else{
	        	imgName =lighttpdPath+"/userimages/"+sender+"."+sender_image_type+"?"+imgTime;
	        }
	        
	        
	        groupMessCount = jsonDataConversation[i].unreadMsgCount;
	        //alert("groupMessCount--"+groupMessCount);
	        if(groupMessCount > 99)
	        	messageCount = "99+";
	        else
	        	messageCount = groupMessCount;
	        	
	        	
	    if(msgType == "groupchat"){
	    	
	         if(sender_image_type == null || sender_image_type=="" || sender_image_type == "null" ){
	        	 	imgName = "";
		        }else{
		        	imgName = lighttpdPath+"/GroupChatImages/"+cid+"."+sender_image_type+"?"+imgTime;
		    	}
	         
	         
		   /* UI+="<li style='float:left;width:99%;cursor:pointer;border-bottom: 1px solid #ced2d5; padding-bottom: 3%; padding-top: 3%;' id='" + cid + "'>" +

	           "<div class='conversation-contact' >" + 
			     "<div style='width: 30px; float: left;'> <img src='"+imgName+"' onerror='groupImageOnErrorReplace(this);' style='width:30px; height:30px; border-radius: 22px;'/></div>" +
		         "<div class='user_box_name' style='font-weight: bold;width:48%;' onclick='openNewGroupChatBox(this)'>" +gname+"</div>";
      		        if(groupMessCount == 0) 
      		        	UI+= "<div class='chatnotification' style='background: #09bcd4; color: #000; float: left; text-align: center; padding: 1%; border-radius: 50%; width: 8%; margin-right: 4%; margin-left: 2%; margin-top: 1%; visibility: hidden;'>1</div>";
				    else
				    	UI+= "<div class='chatnotification' style='background: #09bcd4; color: #000; float: left; text-align: center; padding: 1%; border-radius: 50%; width: 8%; margin-right: 4%; margin-left: 2%; margin-top: 1%;'>"+messageCount+"</div>";	         
      		      UI+= "<div>"+sendTime+"</div> "+  
		         "</div><div style='padding-left: 16%;'>"+msg+"</div></li>";*/
	  
	         UI+="<li onclick='openNewGroupChatBox(this);'style='float:left;width:99%;cursor:pointer;border-bottom: 1px solid #ced2d5; padding-bottom: 1%; padding-top: 0%;' id='" + cid + "'>" 
	         +"<div class='row conversation-contact' style='height: 70px; min-height: 70px;'>"
	 		+"<div class='col-xs-3'>"
	 		 +"<img src='"+imgName+"' title='"+gname+"' onerror='groupImageOnErrorReplace(this);' class='img-square conversationContactImage' style='height: 59px; margin-left: -10px; margin-top: 4px; width: 59px;border-radius: 50%;'>"	
	 		+"</div>"
	 		+"<div class='col-xs-9 conversationContactRow' style='height: 70px; padding-left: 5px;'>"
	 			+"<div class='row' style='height: 20px; min-height: 20px;'>"
	 				+"<div class='col-xs-10 user_box_name  conversationContactName'  style='font-family: OpenSansRegular; margin: 1px 0px 0px 15px; height: 20px; font-size: 13px; padding-left: 0px;width:auto'>" +gname+"</div>";
	        UI+="</div>"
	 			+"<div class='row' style='font-family: OpenSansItalic; font-size: 10px;'>"
	 			+"<div class='col-xs-12 coversationGroupContactTime' style='font-size: 10px; min-height: 20px; height: 20px; margin-top: 3px;'>"+sendTime+"</div>"
	 			
	 			+"</div>"
	 			+"<div class='row'>"
	 			+"<div class='col-xs-10 coversationGroupContactMsg defaultWordBreak' style='font-family: OpenSansRegular; font-size: 15px; height: 20px; margin-top: -3px;'>"+msg+"</div>"
	 			 if(groupMessCount == 0)
	        	 UI+="<div class='col-xs-1-offset badge chatnotification' style='background-color: rgb(255, 51, 51); margin-left: 0px; float: right; '></div>";
	         else	
	        	 UI+="<div class='col-xs-1-offset badge chatnotification' style='background-color: rgb(255, 51, 51); margin-left: 0px; float: right;'>"+messageCount+"</div>";
	         
	 			UI+="</div>"
	 			+"</div>"
	 			+"</div></li>";
		      
	     }else{
	     var test;
	     var jid_id = sender+"-"+chatDomain+"-colabus-com";
	     
	   		if($( "#convoListContainer #"+jid_id).find(".user_box_status").hasClass("user_box_status_offline")){
	   			test ="user_box_status_offline";
	   		}else if ($( "#convoListContainer #"+jid_id).find(".user_box_status").hasClass("user_box_status_online")){
	   			test ="user_box_status_online";
	   		}else{
	   			test ="user_box_status_offline";
	   		}
	   		
	    /*  UI+="<li id='"+sender+"-devchat-colabus-com' style='float:left;width:99%;cursor:pointer;border-bottom: 1px solid #ced2d5; padding-bottom: 3%; padding-top: 3%;' cid='" + cid + "' onclick='openNewChatBox(this)' >" +
	           "<div class='conversation-contact' >" + 
			     "<div style='width: 30px; float: left;'> <img src='"+imgName+"' onerror='userImageOnErrorReplace(this);' style='width:30px; height:30px; border-radius: 22px;'/></div>" +
		         "<div class='user_box_name' style='font-weight: bold;width:48%;'>" +gname+"</div>" ;
		           if(groupMessCount == 0) 
		        	   UI+="<div class='chatnotification' style='background: #09bcd4; color: #000; float: left; text-align: center; padding: 1%; border-radius: 50%; width: 8%; margin-right: 4%; margin-left: 2%; margin-top: 1%; visibility: hidden;'>1</div>";
				    else
				    	UI+="<div class='chatnotification' style='background: #09bcd4; color: #000; float: left; text-align: center; padding: 1%; border-radius: 50%; width: 8%; margin-right: 4%; margin-left: 2%; margin-top: 1%;'>"+messageCount+"</div>";		         
	            UI+= "<div>"+sendTime+"</div> "+ 
			   "</div><div style='padding-left: 16%;'>"+msg+"</div></li>";*/
	    		
	    		
	    	 UI+="<li  id='"+sender+"-"+chatDomain+"-colabus-com' onclick='openNewChatBox(this);' style='float:left;width:99%;cursor:pointer;border-bottom: 1px solid #ced2d5; padding-bottom: 1%; padding-top: 0%;' id='" + cid + "'>" 
	         +"<div class='row conversation-contact' style='height: 70px; min-height: 70px;'>"
	 		+"<div class='col-xs-3'>"
	 		 +"<img src='"+imgName+"' onerror='userImageOnErrorReplace(this);' title='"+gname+"' class='img-square conversationContactImage' style='height: 59px; margin-left: -10px; margin-top: 4px; width: 59px;border-radius: 50%;'>"	
	 		+"</div>"
	 		+"<div class='col-xs-9 conversationContactRow' style='height: 70px; padding-left: 5px;'>"
	 			+"<div class='row' style='height: 20px; min-height: 20px;'>"
	 				+"<div class='col-xs-10  user_box_name conversationContactName' style='font-family: OpenSansRegular; margin: 1px 0px 0px 15px; height: 20px; font-size: 13px; padding-left: 0px;width:auto'>" +gname+"</div>"
	 				+"<div class='user_box_status "+test+" ' style='margin-top:1.3vh;float:right'></div>";
	 				
	 				
	 				UI+="</div>"	
	 			+"<div class='row' style='font-family: OpenSansItalic;'>"
	 			+"<div id='coversationContactTime_"+sender+"' class='col-xs-12 coversationContactTime' style='font-size: 10px; min-height: 20px; height: 20px; margin-top: 3px;'>"+sendTime+"</div>"
				+"</div>"
	 			+"<div class='row'>"
	 			+"<div id='conversationContactMsg_"+sender+"' class='col-xs-10 conversationContactMsg defaultWordBreak' style='font-family: OpenSansRegular; font-size: 15px; height: 20px; margin-top: -3px;'>"+msg+"</div>"
	 			+"<div id='conversationContactMsg1_"+sender+"' class='col-xs-10  defaultWordBreak' style='display:none;color:darkgreen;font-family: OpenSansRegular; font-size: 15px; height: 20px; margin-top: -3px;'>"+msg+"</div>"
	 			if(groupMessCount == 0)
	 		        	 UI+="<div id='conversationMsgCount_"+sender+"' class='col-xs-1-offset badge chatnotification' style='background-color: rgb(255, 51, 51); margin-left: 0px; float: right; '></div>";
	 		         else	
	 		        	 UI+="<div id='conversationMsgCount_"+sender+"' class='col-xs-1-offset badge chatnotification' style='background-color: rgb(255, 51, 51); margin-left: 0px; float: right; '>"+messageCount+"</div>";
	 		         
	 			UI+="</div>"
	 			+"</div>"
	 			+"</div></li>";
	    	 
		    
	   
	  // UI=UI.toString().replaceAll("CH(70)","\\").replaceAll("CHR(26)",":").replaceAll("CH(51)","\"").replaceAll("CHR(26)", ":").replaceAll("CHR(41)", " ").replaceAll("CH(39)", " ").replaceAll("CH(52)","'").replaceAll("CHR(39)", "'").replaceAll("CHR(33)", "/").replaceAll('CH(50)','<br>');
	}
   }	
    resizeChatWindow();
    var windowWidth = $(window).width();
	 if(windowWidth < 750){
		 $(".conversationContactRow").css({'padding-left':'5px'});
		 $(".chatnotification").css({'margin-left':'0px'});
		 
		 }else{
			// $(".conversationContactRow").css({'padding-left':'0px'});
			 $(".chatnotification").css({'margin-left':'17px'});
			 $(".conversation-contact").css({'height':'10vh'});
		/* $(".conversationContactImage").css({'height':'8vh','width':'8vh'});
		 $(".conversationContactName").css({'font-size':'0.9em','height':'3vh','margin':'3px 0 0 15px'});
		 $(".coversationContactTime").css({'font-size':'0.7em','height':'3.5vh','margin-top':'0px'});
		 $(".coversationGroupContactTime").css({'font-size':'10px','height':'3.5vh','margin-top':'2px','padding-top':'0.3vh',});
		 $(".conversationContactMsg").css({'font-size':'1em','height':'3vh','margin-top':'-8px'});
		 $(".groupUser").css({'margin-top':'2%'});
		 $(".conversationContactRow").css({'padding-left':'0px','margin-left':'-2vh'});
		 $(".coversationGroupContactMsg").css({'font-size':'1em','height':'3vh','margin-top':'-6px'});	*/ 
		 }
		 //resizeChatWindow();
    return UI;
    
    
}






//loadProjectTabData
