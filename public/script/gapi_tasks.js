  var createdby='';
  var email='';
  var taskDetails = '';
  var taskStatus='NotCompleted';
  var taskListId=[];
  var tasksIds=[];
  var flag=0			  
 
 function switchToGoogleTasks() {					
 	sortTaskType='googleTasks';
    $('#mytaskdatalist').empty();
  	$('#mytaskdatalist').css('width','99.5%');
  	$('img#googleTaskImg').hide();
  	$('#assignedTaskContainer,#historyContainer').hide();
  	$('.googleColabusTask').show();	$('#colabusTaskImg').show();
  	$('#googleTask').hide();
  	 $("#taskContentDiv").hide();
  	$('#myTaskSection').attr('onClick','');
  	$('#selectDropDownDiv').hide();
  	gapi.client.setApiKey(apiKey);
    window.setTimeout(checkAuth,1);
  }

  function checkAuth() {
    gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
  }

  function handleAuthResult(authResult) {
    if (authResult && !authResult.error)
      makeApiCall();
    else
    	handleAuthClick();
  }

  function handleAuthClick(event) {
  	gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
    //console.log(gapi.auth.getToken());
    return false;
  }

  function makeApiCall() {
  	taskDetails = '';
	gapi.client.load('oauth2', 'v2', function() {
  	  parent.$('#loadingBar').show();	
	  parent.timerControl("start");	
	  gapi.client.oauth2.userinfo.get().execute(function(resp) {
	    //console.log(resp.name);
	    createdby=resp.name;
	    email=resp.email;
	    gapi.client.load('tasks', 'v1', function(res) {
	    	gapi.client.request({
	          'path': '/tasks/v1/users/@me/lists',
	          'callback': handleListTasklists
	        });
		});
	  });
	});
  }
  
  function handleListTasklists(response) {
     var tasklists = response && response.items;
     if (!tasklists || !tasklists.length) {}
     else{
     	tasklistId='';
	 	$(tasklists).each(function( index ) {
		     tasklistId = tasklists[index].id;
		     tasksLists.push(tasklistId);
		}); 
     }
     pointer=0;
     count =tasksLists.length 
     if(count >0){
     	getTasks(pointer);
     }
  }
  
  function getTasks(pointer){
	  gapi.client.request({
          'path': '/tasks/v1/lists/' + tasksLists[pointer] + '/tasks',
          'method': 'GET',
          'callback': handleTasks
	  }); 
  }

  function handleTasks(response) {
  	var tasks = response && response.items;
  	if (!tasks || !tasks.length) {}
  	else{
  		$(tasks).each(function( index ) {
	 		task = tasks[index].id;
	 		tasksIds.push(task);
	 		taskLIstMap[task]=tasksLists[pointer];
	 	}); 
	 }
	 pointer++;
	 if(pointer<count)
	     getTasks(pointer);
	 else{
	     count =tasksIds.length ;
	     pointer=0;
         if(count >0)
     	 	getTaskDetails(pointer);
     	 else{
     	 	taskDetails = "<div style=\"cursor:pointer;width:100%;color:#FFFFFF;text-align:center\">No Task Available</div>";
     	 	$('#mytaskdatalist').append(taskDetails);
     	 	googleTaskScroll();
  			return false;
     	 }
     	 		
     }
  }
  
  function getTaskDetails(pointer){
  	taskListId = taskLIstMap[tasksIds[pointer]];
    gapi.client.request({
      'path': '/tasks/v1/lists/' + taskLIstMap[tasksIds[pointer]] + '/tasks/' + tasksIds[pointer] ,
      'method': 'GET',
      'callback': handleTaskDetails
    }); 
  }

  function handleTaskDetails(response) {				
  	if(response.id){ /* && flag == 0*/
  		var date = '';
  		var time = '';
  		if(response.due){
	  		date = $.datepicker.formatDate('dd-M-yy', new Date(response.due));
	  		time = "00:00:00 AM";
	  		if(response.status == 'completed')
	  			taskStatus="Completed";
	  		else if(Date.parse(response.due) >= Date.parse(new Date()))
	  			taskStatus="InProgress";
	  		else
	  			taskStatus="NotCompleted";	
  		}else{
  			date = '-';
  			time = '-';
  		}
  		taskDetails = "<div style=\"cursor:pointer;width:100%\" class=\"docListViewBorderCls\" onclick=\"viewGoogleTaskPopup('"+response.title+"','"+response.notes+"','"+email+"','"+createdby+"','"+date+"','"+time+"','"+taskStatus+"');\"><div style=\"padding-top:6px\">"+
  					  "<div class=\"rTasksType\" align=\"left\" vaign=\"middle\" style=\" width:5%;float:left;margin-left:10px;margin-top:5px;\"><img title=\"Google Task\" class=\"taskTypeImg\" style=\"float:left;margin-left:8px;\" src=\"images/calender/googleTask.png\" /></div>"+
            	 	  "<div  class=\"rTasksPriority\" align=\"left\" vaign=\"middle\" style=\" width:7%;float:left;margin-top:7px;\"><div><img title=\"none\" src=\"images/calender/none.png\"></span><span class=\"hidden_searchPriority\" style=\"display:none\">6</span></div></div>"+
            	 	  "<div  class=\"rTasksName\" align=\"left\" vaign=\"middle\" style=\" width: 25%;float:left;margin-top:6px;\" ><div><span class=\"taskSub\" title=\""+response.title+"\">"+response.title+"</span><span class=\"hidden_searchTask\" style=\"display:none\">"+response.title.toLowerCase()+"</span></div></div>"+
            	 	  "<div style=\"float:left;width:22%\" class=\"rTasksOwner\" dtaskid=\""+response.id+"\" taskstatus=\"\" taskid=\""+response.id+"\" ><div><span style=\" color: #000000;font-weight:normal;height: 20px;overflow-x: hidden;float:left;margin-top:8px;\" title=\""+email+"\" class=\"feedExtraFont taskAT\">"+createdby+"</span><span style=\"display:none\" class=\"hidden_searchCreatedBy\">"+createdby+"</span>"+
            	 	  "</div></div>"+
                      "<div class=\"rTasksOwner\" align=\"left\" vaign=\"middle\" style=\" float: left; width: 12%;margin-top:6px;\"><span title=\""+email+"\">"+createdby+"</span><span class=\"hidden_searchCreatedBy\" style=\"display:none\">"+createdby.toLowerCase()+"</span></div>"+
                      "<div class=\"rTasksDate\" align=\"left\" vaign=\"middle\" style=\" float: left; width: 11%;\">-<span class=\"hidden_searchDate\" style=\"display:none\">-</span></div>"+ 
                      "<div class=\"rTasksDate\" align=\"left\" vaign=\"middle\" style=\" float: left; width: 11%;\">"+date+"<span class=\"hidden_searchDate\" style=\"display:none\">"+date.toLowerCase()+"</span><div>"+time.toLowerCase()+"</div></div>"+
                      "<div class=\"rTasksStatus\" align=\"left\" vaign=\"middle\" style=\"width:5%;float:left;margin-top:5px;\"><img title=\""+taskStatus+"\" style=\"margin-left:15px;\" src=\"images/calender/"+taskStatus+".png\" /></div>"+
                      "<input type=\"hidden\" id=\"googleTaskListId\" value=\""+taskListId+"\" /><input type=\"hidden\" id=\"googleTaskId\" value=\""+response.id+"\" />"+
  					  "</div></div>";
  		/*$('.rTasksOwner').each(function(obj){
	  		if(response.id == $(this).attr('dtaskid'))
	  			flag=1;//return false;
	  			var date = new Date();
				var timeZone = date.getTimezoneOffset();
				alert(date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + "T" + date.getHours() + ":" + date.getMinutes()  + ":" + date.getSeconds() + "." + date.getMilliseconds() + (timeZone > 0 ? "-" : "+") + Math.floor(Math.abs(timeZone) / 60) + ":" + Math.abs(timeZone) % 60);
  		});
   		*/
  		$('.rTasksOwner').each(function(obj){
	  		if(response.id == $(this).attr('dtaskid'))
	  			flag=1;//return false;					
  		});
  		if(flag==0){		  
  			$('#mytaskdatalist').append(taskDetails);
  		}else{
  			googleTaskScroll();
  			return false;
  		}	
  	}
  	else{
  		addTaskGoogle();
  		return false;
  	}
    pointer++;
    if(pointer<count)
	    getTaskDetails(pointer);
	else{
		googleTaskScroll()
	}        
  }
  
  function googleTaskScroll(){
  		flag=0;
  		tasksIds=[];
		taskListId=[];
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
    	if(!isiPad){
	        $('#mytaskdatalist').mCustomScrollbar("destroy");
		  	$('#mytaskdatalist').mCustomScrollbar({
			  scrollButtons:{
				  enable:true
			  }
		    });	
		    $('#mytaskdatalist .mCSB_container').css('margin-right','15px');
    	}else {
        	$('#mytaskheaders').css('width','100%');	
        	$("#mytaskdatalist").scroll(function(){
 				if($("#mytaskdatalist").scrollTop() >= ($("#MytaskList").height() - $("#mytaskdatalist").height()) && myTaskScroll == true)
                {	
                	$('#mytaskdatalist').mCustomScrollbar("destroy");
				  	$('#mytaskdatalist').mCustomScrollbar({
					  scrollButtons:{
						  enable:true
					  }
				    });
				    $('#mytaskdatalist .mCSB_container').css('margin-right','15px');
		          }
		     }); 
        }
        parent.$('#loadingBar').hide();	
		parent.timerControl("");
  }
  
  function addTaskGoogle() {
  	var listId=$('#googleTaskListId').val();
	  var task = {
	    title: 'Pick up dry cleaning',
	    notes: 'Remember to get this done!',
	    due:'2014-04-11 11:59:00'
	  };
	  task = Tasks.Tasks.insert(task, listId);
	  Logger.log('Task with ID "%s" was created.', task.id);
  }
  
  function viewGoogleTaskPopup(title,desc,email,createdBy,date,time,status){
  	parent.$("#transparentDiv").show();
  	$("#gTaskPopup").show();
  	$('#gTaskTitle').html(title);
  	$('#gTaskTileDiv').val(title);
  	$('#gTaskInstrDiv').val(desc);
  	$('#gTaskTaskDueDateDiv').val(date);
  	$('#gTaskAssgndToDiv,#gTaskCreatedByDiv').html(createdBy);
  	$('#gTaskAssgndToDiv,#gTaskCreatedByDiv').attr('title',email);
  	$('#gTaskStatusDiv').val(status);
  	if(status == 'Completed')
  		$('select#gTaskStatusSelect').val(status);
  	else
  		$('select#gTaskStatusSelect').val("NotCompleted");
  	initGoogleTaskCal();
  }

  function closeGTaskPopup(){
  	parent.$("#transparentDiv").hide();
  	$("#gTaskPopup").hide();	
  }	