/* globals hopscotch: false */

/* ============ */
/* EXAMPLE TOUR */
/* ============ */
var tour = {
  id: 'hello-hopscotch',
  steps: [
    {
      target: 'startTourBtn',
      title: 'Welcome to Colabus!',
      content: 'Hey there! Start off by creating your own project.',
      placement: 'right',
      arrowOffset: 65,
      yOffset: 120, 
      xOffset: -310
      
    },
    {
      target: 'viewExistingProject',
      title: 'View Your Project',
      content: 'You can view the project you created in this tab!',
      placement: 'bottom',
      arrowOffset: 65
    },
    {
      target: 'task',
      placement: 'top',
      title: 'Create New Task',
      content: 'Create Tasks! Task groups form to solve a business problem, whether it\'s increasing sales, improving customer relations or ramping up productivity. When employees come together to solve problems, they often find the arrangement advantageous'
    },
    {
      target: 'doc',
      placement: 'left',
      title: 'All your documents in a single place!',
      content: 'Have all your documents in a single place no fuss, easy to find.',
      yOffset: -10, 
      arrowOffset: 5
    },
    {
      target: 'msg',
      placement: 'right',
      title: 'Send out messages',
      content: 'One click access to all your messages.',
      yOffset: -15,
      xOffset: -150
      
    },
    {
      target: 'note',
      placement: 'top',
      title: 'Make notes',
      content: 'Create, Maintain and Share your notes.',
    },
    {
      target: 'blog',
      placement: 'bottom',
      title: 'Post your blogs',
      content: 'Post your blogs, share your thoughts!',
    },
    {
      target: 'gallery',
      placement: 'left',
      title: 'Gallery!',
      content: 'Upload, share your moments!',
      yOffset: -15
    },
    
	{
      target: 'analytics',
      placement: 'left',
      title: 'Reports',
      content: 'Look at a detailed report of  the task status.',
      yOffset: -15
    },
    {
      target: 'NotfDataRight',
      placement: 'left',
      title: 'Notification',
      content: 'Have a look at all your real time notifications',
      yOffset: -15
    },
    {
      target: 'listMenuIcon',
      placement: 'bottom',
      title: 'Menus',
      content: 'One click access to all menus!',
      yOffset: -15
    },
    {
      target: 'demo-b',
      placement: 'left',
      title: 'Global Search',
      content: 'Search all your files globally',
    },
     {
      target: 'chatIcon',
      placement: 'bottom',
      title: 'Chat',
      content: 'Instant messaging is a click away',
      xOffset: -200,
      arrowOffset: 205
    }
    
    
  ],
  showPrevButton: true,
  scrollTopMargin: 100
},

/* ========== */
/* TOUR SETUP */
/* ========== */
addClickListener = function(el, fn) {
  if (el.addEventListener) {
    el.addEventListener('click', fn, false);
  }
  else {
    el.attachEvent('onclick', fn);
  }
},

init = function() {
  var startBtnId = 'startTourBtn',
      calloutId = 'startTourCallout',
      mgr = hopscotch.getCalloutManager(),
      state = hopscotch.getState();

  if (state && state.indexOf('hello-hopscotch:') === 0) {
    // Already started the tour at some point!
    hopscotch.startTour(tour);
  }
  else {
    // Looking at the page for the first(?) time.
    setTimeout(function() {
      mgr.createCallout({
        id: calloutId,
        target: startBtnId,
        placement: 'right',
        title: 'Take an example tour',
        content: 'Start by taking an example tour to see Colabus in action!',
        yOffset: -25,
        arrowOffset: 15,
        width: 240,
        display:none
      });
    }, 100);
  }

  addClickListener(document.getElementById(startBtnId), function() {
    if (!hopscotch.isActive) {
      mgr.removeAllCallouts();
      hopscotch.startTour(tour);
    }
  });
};

//condition to check here whether the init function has to be called
init();

